#!/bin/bash

# Source: https://stackoverflow.com/a/3474556/4106848
# Stop the execution of the script if a command has a non-zero exit code
set -e

# Source: https://stackoverflow.com/a/246128/4106848
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

echo "== Creating a backup =="
"$SCRIPT_DIR/backup.sh" .backup/update 1
echo ""

echo "== Fetching the latest changes from Git =="
git fetch
git pull
echo ""

echo "== Fetching the latest version of Docker containers =="
docker compose pull
echo ""

echo "== Building the new Docker image for our application =="
docker compose build
echo ""

echo "== Restarting containers =="
docker compose up -d
echo ""

echo "== Update finished =="
