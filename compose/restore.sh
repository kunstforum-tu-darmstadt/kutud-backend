#!/bin/bash

# Source: https://stackoverflow.com/a/3474556/4106848
# Stop the execution of the script if a command has a non-zero exit code
set -e

# Parameters
if [ "$#" -ne 1 ]
then
  echo "Usage: ./restore.sh <backup-file>"
  exit 1
fi

# Continue prompt
# https://stackoverflow.com/a/27875395/4106848
echo -n "WARNING: Restoring the data will erase all current data! Continue (y/n)? "
read -r CONTINUE_ANSWER

if ! [ "$CONTINUE_ANSWER" != "${CONTINUE_ANSWER#[Yy]}" ]; then
  exit 1
fi

# Source: https://stackoverflow.com/a/246128/4106848
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ENV_FILE="$SCRIPT_DIR/.env"

# Read the docker compose .env file
if [ -f "$ENV_FILE" ];
then
  source "$ENV_FILE"
else
  echo "No .env file found"
  exit 1
fi

# Create a new backup of the current state before applying the old backup
echo "== Creating a backup of the current state =="
"$SCRIPT_DIR/backup.sh" .backup/restore 1
echo ""

# Variables
CONTAINER_MARIADB="${COMPOSE_PROJECT_NAME}-mariadb-1"
CONTAINER_DJANGO="${COMPOSE_PROJECT_NAME}-django-1"
BACKUP_PATH="$1"
CONTAINER_TMP_SQL="/tmp/$DB_NAME.sql"

# Create a temporary directory
TMP_DIR=$(mktemp -d)

# Collecting files to backup
echo "== Extracting old backup =="
tar -xf "$BACKUP_PATH" -C "$TMP_DIR"

echo "== Applying old backup =="
# Restoring the database
# https://stackoverflow.com/a/34791230/4106848
docker cp "$TMP_DIR/$DB_NAME.sql" "$CONTAINER_MARIADB:$CONTAINER_TMP_SQL"
docker exec "$CONTAINER_MARIADB" /bin/sh -c "mysql -u$DB_USER -p'$DB_PW' $DB_NAME < $CONTAINER_TMP_SQL"
# Restoring media files
docker exec "$CONTAINER_DJANGO" /bin/sh -c "rm -rf /opt/media/*"
docker cp "$TMP_DIR/media" "$CONTAINER_DJANGO:/opt/"

# Remove the temporary files
echo "== Cleaning up temporary files =="
rm -r "$TMP_DIR"

echo "== Restore process finished =="
