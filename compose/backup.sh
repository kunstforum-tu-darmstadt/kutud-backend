#!/bin/bash

# Parameters
if ! { [ "$#" -eq 1 ] || [ "$#" -eq 2 ]; };
then
  echo "Usage: ./backup.sh <target-directory> [hold-days]"
  echo "Omit the hold-days argument to keep backups indefinitely"
  exit 1
fi

# Source: https://stackoverflow.com/a/246128/4106848
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
ENV_FILE="$SCRIPT_DIR/.env"

# Read the docker compose .env file
if [ -f "$ENV_FILE" ];
then
  source "$ENV_FILE"
else
  echo "No .env file found"
  exit 1
fi

# Variables
TIMESTAMP=$(date +"%F--%H-%M")
CONTAINER_MARIADB="${COMPOSE_PROJECT_NAME}-mariadb-1"
CONTAINER_DJANGO="${COMPOSE_PROJECT_NAME}-django-1"
BACKUP_DIRECTORY="${1%/}"
HOLD_DAYS="$2"
ARCHIVE_PATH="$BACKUP_DIRECTORY/$TIMESTAMP.tar.gz"

# Create a temporary directory
TMP_DIR=$(mktemp -d)

# Collecting files to backup
echo "== Collecting files =="
docker exec "$CONTAINER_MARIADB" /bin/sh -c "/usr/bin/mysqldump -u$DB_USER -p'$DB_PW' $DB_NAME" > "$TMP_DIR/$DB_NAME.sql"
docker cp "$CONTAINER_DJANGO":/opt/media/ "$TMP_DIR/media"
echo ""

# Create a archive with the files
# Source: https://stackoverflow.com/a/3035446/4106848
echo "== Creating archive $ARCHIVE_PATH =="
mkdir -p "$BACKUP_DIRECTORY"
tar -czf "$ARCHIVE_PATH" -C "$TMP_DIR" .
echo ""

# Remove the temporary files
echo "== Cleaning up temporary files =="
rm -r "$TMP_DIR"
echo ""

# Remove older backups if enabled
if [ "$#" -eq 2 ]
then
  echo "== Removing backups older than $HOLD_DAYS days =="
  find "$BACKUP_DIRECTORY" -maxdepth 1 -mindepth 1 -type f -mtime +"$HOLD_DAYS" -regex ".*\.tar\.gz" -printf "%p\n" -exec rm {} \;
  echo ""
fi

echo "== Backup finished =="
