# Setup & Update

The application can be set up and kept up to date on a wide range of servers thanks to Docker Compose. 

## Installation

If you want to install this application for production usage, you can utilize 
the [`docker-compose.yml`](../compose/docker-compose.yml) configuration file 
which automatically installs all other required services to run this application. 
To begin, follow the six steps:

1. Install [Docker Engine](https://docs.docker.com/engine/install/#server)
2. Install [Docker Compose](https://docs.docker.com/compose/install/)
3. Copy the [`compose/.env.dist`](../compose/.env.dist) file to a local `compose/.env` file
4. Set the required passwords
5. Build the `kunstforum` image using `docker compose build`
6. Start all services (including the application) using `docker compose up -d`
7. Create a superuser account using `docker compose exec django python manage.py createsuperuser`
8. Access the administrative interface with the superuser account at `https://${DOMAIN}/admin`.

This setup uses [MariaDB](https://mariadb.org/) as a database and [Redis](https://redis.io/) for caching.
To employ other [database](https://docs.djangoproject.com/en/4.0/ref/databases/) or 
[caching](https://docs.djangoproject.com/en/4.0/topics/cache/) solutions,
it is important to change both `DATABASE_URL` and `CACHE_URL` in [`docker-compose.yml`](../compose/docker-compose.yml)
according to the documentation linked above.
It also may be necessary to install additional Python packages.

## Update

You can update the application with the provided bash script.

Please ensure before updating that you didn't modify any code, otherwise Git may stop the process.
Please also note that the update only pulls the latest version from the current branch you are on.
Therefore, you might have to switch branches before updating to get the expected result.

A backup is created before the update begins.
You can find the backup in the folder `compose/.backup/update`.
The backup is intended to be kept for one day.
It will be deleted as soon as you execute the script again at least 24 hours later.

1. Go to the [`compose`](../compose) folder using `cd compose`
2. Start the update with `./update.sh`
