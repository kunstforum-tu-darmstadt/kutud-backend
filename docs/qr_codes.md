# QR Codes

The admin interface provides the functionality to generate QR codes for artworks.
They can be printed and attached to physical artworks.

If QR codes are scanned with a generic QR code scanner app, they link to a configurable website.
If they are scanned using the [Kunstforum app](https://git.rwth-aachen.de/kunstforum-tu-darmstadt/KuTUD), the artwork's detailed information is opened inside the app.

An example QR code for the website `https://www.tu-darmstadt.de/kunstforum/index.de.jsp#` with the artwork identifier `K11` can be seen below.
![Example QR code](images/qr-code-7-k11.svg)

## Configure URL

Before you can start generating QR codes, you have to configure a URL.
The URL and the artwork identifier are combined to generate the text encoded within the QR code.

The URL must start either with the `http://` or the `https://` protocol and must end with one of the three characters of `#`, `=` or `/`.

The character `#` tells web browsers to ignore the appended artwork identifier.
If you are unsure which end character to choose, use this character.

The characters of `=` and `/` allow the website to process the appended artwork identifier.
They can be used for example to display a preview of the artwork directly in the webbrowser.
A custom implementation on the linked website is required for these functionalities to work properly.

An example URL is `https://www.tu-darmstadt.de/kunstforum/index.de.jsp#`.
You can configure the URL as follows.
1. Click the `+ Add` button in the `App information` column on the main page
2. Select `Website for the app` as the `Key`
3. Enter a URL in the field `Text` according to the rules explained in the section [Configure URL](#configure-url)
4. Click `Save`

You can change the URL without breaking the functionality of the Kunstforum app for existing QR codes.
Only artwork identifiers after the characters `#`, `=` or `/` are processed to open the artwork.

When generating QR codes, the artwork identifier is [URL encoded](https://en.wikipedia.org/wiki/Percent-encoding) and appended to the configured URL.

## Generate

Multiple QR codes for artworks can be generated at once.
1. Go to the list view of `Artworks`
2. Select multiple artworks with non-empty identifiers using the checkboxes on the left
3. Scroll up and select the action `Generate & download QR codes`
4. Click the button on the right named `Go`
5. Wait for the download to start

Inside the downloaded ZIP file with the name `qr-codes.zip` you'll find serval SVG files.
The name of each SVG file contains the numeric database identifier and the textual identifier chosen by you.
