# API Documentation

A quick explanation by example of all accessible endpoints.
This API is designed as a [RESTful API](https://www.redhat.com/en/topics/api/what-is-a-rest-api).

All timestamps are served as [unix timestamps](https://en.wikipedia.org/wiki/Unix_time).

**Actual order of key-value pairs served by API may differ!**

Pagination is used to enable partial loading of results where needed
while also enabling the server to have shorter response times as
the responses themselves are smaller.
The default page size is 50, though it can be increased up to 100 by specifying a `?page_size=` GET-parameter.
Previous and next pages are provided by link.
If no next or previous page exists, `null` is returned.

`null` is also returned when optional fields have not been filled with information by the editors.

## Endpoints
- GET `/api`
- GET `/api/exhibitions[?page={page_number}]`
  - GET `/api/exhibitions/{exhibition_id}`
- GET `/api/artworks[?page={page_number}]`
  - GET `/api/artworks/{artwork_id}`
- GET `/api/artists[?page={page_number}]`
  - GET `/api/artists/{artist_id}`
- GET `/api/media` (418)
  - GET `/api/media/{media_uuid}`
- GET `/api/app_information`

### /api
```json
{
    "status":"online"
}
```
### /api/exhibitions
```json
{
    "count":3,
    "next":"http://testserver/api/exhibitions/?page=2",
    "previous":null,
    "results":[
        {
            "title":"Exhibition",
            "id":1
        },
        {
            "title":"Exhibition",
            "id":2
        }
    ]
}
```

#### /api/exhibitions/{exhibition_id}
```json
{
    "id":1,
    "artworks":[
        2,
        1
    ],
    "cover_image":"ed385753-1445-4674-aea1-d02d0ac6683a",
    "created_at":1641898051,
    "updated_at":1641898051,
    "title":"Exhibition",
    "start_date":1641859200,
    "end_date":1641859200,
    "artwork_count":2
}
```

### /api/artworks
The attribute `identifier` indicates the identifier that is manually added by the respective editor.
In the app, one can only search by 'identifier', **not** by id.

```json
{
    "count":3,
    "next":"http://testserver/api/artworks/?page=2",
    "previous":null,
    "results":[
        {
            "name":"Artwork",
            "id":1,
            "identifier":"1"
        },
        {
            "name":"Artwork",
            "id":2,
            "identifier":"2"
        }
    ]
}
```

#### /api/artworks/{artwork_id}
*Optional fields*: `year`, `type`, `canvas`, `artist`, `owner`, `acquired`, `linked_artwork`, `audio_guide`,
`video_link`, `latitude`, `longitude`, `location`, `address`, `campus`

```json
{
    "id":1,
    "images":[
        "7b665c4e-5584-4e52-b3db-01263a20bfcb",
        "d1129c8c-bcad-4308-8ac3-6b388f6edb99",
        "ed385753-1445-4674-aea1-d02d0ac6683a"
    ],
    "cover_image":"7b665c4e-5584-4e52-b3db-01263a20bfcb",
    "audio_guide":"1bdcdb5f-f475-43db-b058-39f80fad5390",
    "owner":"Kunstforum",
    "campus":"Lichtwiese",
    "linked_artwork":3,
    "created_at":1641898051,
    "updated_at":1641898051,
    "identifier":"123",
    "name":"Artwork",
    "year":1853,
    "type":"painting",
    "canvas":"Oil on canvas",
    "info_text":"It looks nice.",
    "video_link":"https://www.youtube.com/watch?v=jNQXAC9IVRw",
    "latitude":"49.8773455",
    "longitude":"8.6547907",
    "location":"S2|02 Robert Piloty Gebäude, Außengelände",
    "address":"Hochschulstraße 10",
    "artist":2,
    "image_count":3,
    "how_acquired":"Stolen"
}
```

### /api/artists
```json
{
    "count":3,
    "next":"http://testserver/api/artists/?page=2",
    "previous":null,
    "results":[
        {
            "name":"Picasso",
            "id":1
        },
        {
            "name":"Picasso",
            "id":2
        }
    ]
}
```

#### /api/artists/{artist_id}
*Optional fields*: `year_birth`, `year_death`, `info_text`

```json
{
    "id":1,
    "created_at":1641898051,
    "updated_at":1641898051,
    "name":"Picasso",
    "year_death":1973,
    "info_text":"Picasso war ein toller Künstler",
    "year_birth":1881
}
```

### /api/media
HTTP 418 I'm a teapot (Content: `{}`)

#### /api/media/{media_uuid}
```json
{
    "uuid":"7b665c4e-5584-4e52-b3db-01263a20bfcb",
    "created_at":1641898051,
    "updated_at":1641898051,
    "description":"An image",
    "copyright":"Marcel Davis",
    "category":"image",
    "type":"image/png",
    "url":"http://testserver/media/image.png"
}
```
### /api/app_information
```json
[
    {
        "key":"group",
        "created_at":1641898051,
        "updated_at":1641898051,
        "text":"Ein testweiser Text"
    },
    {
        "key":"art_at_university",
        "created_at":1641898051,
        "updated_at":1641898051,
        "text":"Ein testweiser Text"
    },
    {
        "key":"imprint",
        "created_at":1641898051,
        "updated_at":1641898051,
        "text":"Ein testweiser Text"
    }
]
```
