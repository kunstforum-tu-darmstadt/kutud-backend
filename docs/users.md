# Groups & Users

This application utilizes the permission system included with Django and defines some default groups.

## Groups

The groups are ranked by their level of power.

The user created in the last step of the setup process, also called superuser, 
has equivalent permissions to an administrator although the user is not assigned to the group.

1. Administrator: Can add, edit and delete all objects in the database including groups & users.
2. Editor-in-Chief: Can add, edit and delete all objects related to the app's content management, excluding groups & users.
3. Editor: Can only add new and edit assigned objects related to the app's content management. 
They cannot publish exhibitions or artworks.

The permissions of groups can be edited by administrators if desired, even new groups can be created.

## Users

### Add new accounts

You can add new user accounts with various permission levels using the admin web interface.
1. Open the admin interface located at the `/admin` path of your chosen domain
2. Login with your superuser account (created with the shell)
3. Click the `+ Add` button in the `Users` column
4. Choose username and password
5. Click `Save`
6. Check the box `Staff status` to allow logins to the admin interface
7. Choose a group to assign a permissions level to the user
8. Click `Save`

### Assign editors

Objects are assigned automatically to the user who created them.

The assigment is important for users of the group editor.
They can only edit objects they are assigned to. 

The assignment of an object can be changed.
The purpose is to delegate tasks of editing objects to certain editors.
1. Go to the editing view of the object
2. Scroll down to the section `Internal attributes`
3. Click `Show`
4. Select a new editor
5. Click `Save`

The assigment of multiple objects can be changed at once.
1. Go to the list view of the object
2. Select multiple objects using the checkboxes on the right
3. Scroll up and select the action `Change editor`
4. Click the button to the right named `Go`
5. Choose a new editor 
6. Inspect the selected objects and click `Save`

### Change password

Users can change their password on their own by clicking on the `Change password` 
button in the upper right corner when logged in.

If a user has forgotten their password, an administrator must choose a new one.
1. Go to the editing view of the user in question
2. Click on the small link `this form` in the field `Password`
3. Enter a new password
4. Click `Change Password` to save the chosen password

### Delete accounts

Before deleting an account, consider if just disabling it would be enough.
1. Go to the editing view of the user in question
2. Disable the checkboxes `Staff status` and `Active`
3. Optionally remove all groups and permissions
4. Click `Save`

If you want to fully delete a user account,
be aware that the information about which objects the account created or updated will be lost.
1. Go to the editing view of the user in question
2. Scroll down and click `Delete`
3. Inspect the items that will be deleted
4. Click `Yes, I'm sure`
