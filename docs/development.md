# Development

To develop the application, we recommend setting it up locally.

## Installation

We recommend developing the application locally on your computer.

1. Install [Python 3.10](https://www.python.org/downloads/) or newer
2. Install [pipenv](https://pipenv.pypa.io/en/latest/) for dependency management
3. Install all dependencies using `pipenv install --dev`
4. Set up a local MariaDB database (e.g., as described [here](https://mariadb.com/kb/en/getting-installing-and-upgrading-mariadb/))
5. Copy the [`.env.dist`](.env.dist) file to a local `.env` file
6. Edit it to set the required environment variables
   1. `DEBUG=true`
   2. `SECRET_KEY`
   3. `ALLOWED_HOSTS=.localhost,127.0.0.1,[::1]`
   4. `DATABASE_URL=mysql://user:password@127.0.0.1:3306/kunstforum`
7. Apply database migrations using `pipenv run python manage.py migrate`
8. Create a superuser account using `pipenv run manage.py createsuperuser`
9. Start the application in development mode using `pipenv run manage.py runserver` 

#### Optional step:
Although not directly needed, it might be useful to also set up a
local Redis instance as it enables you to fully mirror a production environment
while still in development:

1. Set up Redis (e.g., as described [here](https://redis.io/topics/quickstart))
2. Set the environment variable
   1. `CACHE_URL=redis://username:password@127.0.0.1:6379` (Note:
by default, neither username nor password are set by Redis - you either
have to do it manually or set the URL to just `redis://127.0.0.1:6379`)

Once the development server is running, edit files and reload the page to instantly see your changes.
