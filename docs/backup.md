# Backup & Restore

The Docker Compose production environment includes backup and restore scripts.
We recommend to regularly create backups.

## Backup

You can create a backup of the database and uploaded files with the provided bash script.
The script accepts two parameters:
```bash
./backup.sh <target-directory> [hold-days]
```
The first parameter is the target folder where the backup archive file will be stored.
The second parameter is optional and allows the deletion of backup files older than the given number of days.

1. Go to the [`compose`](../compose) folder using `cd compose`
2. Start the backup with `./backup.sh .backup/regular 10`

To extract the files inside the backup archive, use the following command `tar -xzvf 2022-01-28--17-05.tar.gz` (example).

To create a backup regularly, you can create a [cronjob](https://crontab-generator.org/). 

## Restore

You can restore backups created with [Backup](#backup).
All edited data will be lost when restoring an old backup.
A new backup is created before the old one is applied.
You can find the new backup in the folder `compose/.backup/restore`. 
The script accepts one parameter:
```bash
./restore.sh <backup-file>
```

1. Go to the [`compose`](compose) folder using `cd compose`
2. Start a backup file with `./restore.sh .backup/regular/2022-01-28--17-05.tar.gz` (example)
