from django.utils import timezone

from management.models import Media, Artist, Owner, Artwork, Exhibition, AppInformation, Campus
from management.models.media_choices import CHOICE_IMAGE, CHOICE_AUDIO_GUIDE


def set_up_image(description: str, copyrights: str = "Marcel Davis") -> Media:
    """
    Creates an image with the supplied description.

    :param description: the image's description
    :param copyrights: the image's copyright description
    :return: the image as a Media object
    """
    return Media.objects.create(
        description=description,
        category=CHOICE_IMAGE,
        file='image.png',
        type='image/png',
        copyright=copyrights
    )


def set_up_artist() -> Artist:
    """
    Creates an artist with all required information.

    :return: the artist as an Artist object
    """
    return Artist.objects.create(
        name='Picasso',
        year_birth=1881,
        year_death=1973,
        info_text='Picasso war ein toller Künstler'
    )


def set_up_artwork(
        identifier: str,
        cover_image: Media,
        artist: Artist,
        published: bool = False,
        campus: Campus = None,
        linked_artwork: Artwork = None
) -> Artwork:
    """
    Creates an artwork with all required objects except for artwork identifier, cover image and artist.
    The published status can be specified if needed.

    :param identifier: the artwork's identifier used for e.g. searching
    :param cover_image: the artwork's cover image for in-app display
    :param artist: the creating artist
    :param published: if the artwork is published or not
    :param campus: the campus the artwork is located on
    :param linked_artwork: another artwork related to this one
    :return: the artwork as an Artwork object
    """
    owner = Owner.objects.create(name='Kunstforum')
    audio_guide = Media.objects.create(
        description='Audio Guide', category=CHOICE_AUDIO_GUIDE, file='audio.mp3', type='audio/mpeg'
    )
    return Artwork.objects.create(
        identifier=identifier,
        name='Artwork',
        published=published,
        year="1853",
        type='painting',
        canvas='Oil on canvas',
        artist=artist,
        owner=owner,
        acquired='Stolen',
        info_text='It looks nice.',
        cover_image=cover_image,
        audio_guide=audio_guide,
        video_link='https://www.youtube.com/watch?v=jNQXAC9IVRw',
        latitude=49.8773455,
        longitude=8.6547907,
        location='S2|02 Robert Piloty Gebäude, Außengelände',
        address='Hochschulstraße 10',
        campus=campus,
        linked_artwork=linked_artwork
    )


def set_up_bare_artwork(identifier: str, cover_image: Media) -> Artwork:
    """
    Creates an artwork with only the required objects.
    The artwork identifier and its cover image must be provided as parameters.
    The newly created artwork is returned
    """
    return Artwork.objects.create(
        identifier=identifier,
        name='Artwork',
        published=False,
        info_text='It looks nice.',
        cover_image=cover_image
    )


def set_up_exhibition(cover_image: Media, published: bool = False) -> Exhibition:
    """
    Creates an exhibition with the given cover image.
    The published status can be specified if needed.

    :param cover_image: the exhibition's cover image for in-app display
    :param published: if the exhibition is published or not
    :return: the exhibition as an Exhibition object
    """
    return Exhibition.objects.create(
        title='Exhibition',
        start_date=timezone.now().date(),
        end_date=timezone.now().date(),
        published=published,
        cover_image=cover_image
    )


def set_up_app_information(key: str) -> AppInformation:
    """
    Creates an app information object with the provided key as specified in the model.

    :param key: the key to identify the text's topic
    :return: the app information as an AppInformation object
    """
    return AppInformation.objects.create(
        key=key,
        text='Ein testweiser Text'
    )


def set_up_campus(name: str) -> Campus:
    """
    Created a campus object with the provided name.

    :param name: the name of the campus
    :return: the campus as a Campus object
    """
    return Campus.objects.create(
        name=name
    )
