import datetime

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from management.models.exhibition import validate_date_not_earlier_than_other


class ExhibitionModelTests(TestCase):
    """
    Tests for the database model Exhibition and its functions.
    """

    def test_validate_date_not_earlier_than_other_with_future_date(self):
        """
        validate_date_not_earlier_than_other() doesn't raise any errors if the start date is earlier than the end date.
        """
        start = timezone.now().date()
        end = (timezone.now() + datetime.timedelta(days=30)).date()
        validate_date_not_earlier_than_other(start, end)

    def test_validate_date_not_earlier_than_other_with_equal_date(self):
        """
        validate_date_not_earlier_than_other() doesn't raise any errors if the start date is equal to the end date.
        """
        start = timezone.now().date()
        validate_date_not_earlier_than_other(start, start)

    def test_validate_date_not_earlier_than_other_with_past_date(self):
        """
        validate_date_not_earlier_than_other() raises a ValidationError if the start date is later than the end date.
        """
        start = timezone.now().date()
        end = (timezone.now() + datetime.timedelta(days=-1)).date()
        self.assertRaisesMessage(
            ValidationError,
            'The end date ({}) is earlier than the start date ({})'.format(end, start),
            validate_date_not_earlier_than_other,
            start,
            end
        )
