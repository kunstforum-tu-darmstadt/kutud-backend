from decimal import Decimal

from django.core.exceptions import ValidationError
from django.test import TestCase

from management.models.artwork import normalize_youtube_url, validate_youtube_url, validate_lat_long_set, \
    validate_no_self_reference
from management.tests.util import set_up_artwork, set_up_bare_artwork, set_up_image, set_up_artist


class ArtworkModelTests(TestCase):
    """
    Tests for the database model Artwork and its functions.
    """

    def setUp(self) -> None:
        """
        Creates an artwork and all required objects and saves them to the database.
        """
        self.image = set_up_image('Image')
        self.image_two = set_up_image('Image 2')
        self.artwork = set_up_artwork("1", self.image, set_up_artist())

    def test_validate_youtube_url_with_long_url(self):
        """
        validate_youtube_url() doesn't raise any errors if the long YouTube-URL links to a video.
        """
        validate_youtube_url('https://www.youtube.com/watch?v=jNQXAC9IVRw')

    def test_validate_youtube_url_with_short_url(self):
        """
        validate_youtube_url() doesn't raise any errors if the short YouTube-URL links to a video.
        """
        validate_youtube_url('https://youtu.be/jNQXAC9IVRw?t=5')

    def test_validate_youtube_url_with_no_video(self):
        """
        validate_youtube_url() raises a ValidationError if the URL doesn't link to a video.
        """
        self.assertRaisesMessage(
            ValidationError,
            'The given URL does not link to a YouTube video. '
            'The URL has to start with https://www.youtube.com/watch?v=',
            validate_youtube_url,
            'https://www.youtube.com'
        )

    def test_validate_youtube_url_with_no_youtube(self):
        """
        validate_youtube_url() raises a ValidationError if the URL doesn't link to a YouTube.
        """
        self.assertRaisesMessage(
            ValidationError,
            'The given URL does not link to a YouTube video. '
            'The URL has to start with https://www.youtube.com/watch?v=',
            validate_youtube_url,
            'https://www.google.com/'
        )

    def test_normalize_youtube_url_with_short_url(self):
        """
        normalize_youtube_url() normalizes a short YouTube URL to a long one.
        """
        short_url = 'https://youtu.be/jNQXAC9IVRw?t=5'
        full_url = 'https://www.youtube.com/watch?v=jNQXAC9IVRw&t=5'
        self.assertEqual(full_url, normalize_youtube_url(short_url))

    def test_normalize_youtube_url_with_long_url(self):
        """
        normalize_youtube_url() returns a full YouTube URL text as is without raising any errors.
        """
        full_url = 'https://www.youtube.com/watch?v=jNQXAC9IVRw&t=5'
        self.assertEqual(full_url, normalize_youtube_url(full_url))

    def test_normalize_youtube_url_with_empty_text(self):
        """
        normalize_youtube_url() returns an empty text as is without raising any errors.
        """
        empty_text = ''
        self.assertEqual(empty_text, normalize_youtube_url(empty_text))

    def test_validate_lat_long_set_both(self):
        """
        validate_lat_long() doesn't raise any errors if both latitude and longitude are set
        """
        validate_lat_long_set(Decimal('12.1234567'), Decimal('12.1234567'))

    def test_validate_lat_long_set_neither(self):
        """
        validate_lat_long() doesn't raise any errors if neither latitude nor longitude are set
        """
        validate_lat_long_set(None, None)

    def test_validate_lat_long_set_either(self):
        """
        validate_lat_long() raises a ValidationError if only latitude or longitude are set
        """
        self.assertRaisesMessage(
            ValidationError,
            'If either latitude or longitude is set, the corresponding other field has to be set as well',
            validate_lat_long_set,
            None,
            Decimal('12.1234567')
        )

        self.assertRaisesMessage(
            ValidationError,
            'If either latitude or longitude is set, the corresponding other field has to be set as well',
            validate_lat_long_set,
            Decimal('12.1234567'),
            None
        )

    def test_validate_no_self_reference_with_none(self):
        """
        validate_no_self_reference() doesn't raise any errors if the artwork does not link to itself.
        """
        artwork_one = set_up_bare_artwork('Art One', self.image)

        validate_no_self_reference(artwork_one)

    def test_validate_no_self_reference_with_other(self):
        """
        validate_no_self_reference() doesn't raise any errors if the artwork links to another artwork.
        """
        artwork_one = set_up_bare_artwork('Art One', self.image)
        artwork_two = set_up_bare_artwork('Art Two', self.image_two)
        artwork_one.linked_artwork = artwork_two

        validate_no_self_reference(artwork_one)

    def test_validate_no_self_reference_with_self(self):
        """
        validate_no_self_reference() raises a ValidationError if the artwork link to itself.
        """
        artwork_one = set_up_bare_artwork('Art One', self.image)
        artwork_one.linked_artwork = artwork_one

        self.assertRaisesMessage(
            ValidationError,
            'The field \'linked artwork\' may not link to the artwork itself',
            validate_no_self_reference,
            artwork_one
        )
