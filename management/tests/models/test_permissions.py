from django.test import TestCase

from management.models.permissions import change_edit_perm, extract_model_name, publish_perm


class PermissionUtilitiesTests(TestCase):
    """
    Tests for the utilities for permissions.
    """

    def test_extract_model_name_with_data(self):
        """
        extract_model_name() extracts the lower case model name of a Meta class.
        """
        self.assertEqual('appinformation', extract_model_name('AppInformation.Meta'))

    def test_extract_model_name_with_None(self):
        """
        extract_model_name() raises a ValueError if the parameter is None.
        """
        self.assertRaisesMessage(
            ValueError,
            'the parameter \'qualified_name\' may not be None or empty',
            extract_model_name,
            None
        )

    def test_extract_model_name_with_empty(self):
        """
        extract_model_name() raises a ValueError if the parameter is just an empty string.
        """
        self.assertRaisesMessage(
            ValueError,
            'the parameter \'qualified_name\' may not be None or empty',
            extract_model_name,
            ''
        )

    def test_change_edit_perm_with_data(self):
        """
        change_edit_perm() returns a permission tuple for the model AppInformation.
        """
        self.assertEqual(
            ('change_editor_appinformation', 'Can change app information if assigned editor'),
            change_edit_perm('AppInformation.Meta', 'app information')
        )

    def test_change_edit_perm_with_None(self):
        """
        change_edit_perm() raises a ValueError if the second parameter is None.
        """
        self.assertRaisesMessage(
            ValueError,
            'the parameter \'verbose_name\' may not be None or empty',
            change_edit_perm,
            'AppInformation.Meta',
            None
        )

    def test_publish_perm_with_data(self):
        """
        publish_perm() returns a permission tuple for the model Artwork.
        """
        self.assertEqual(
            ('publish_artwork', 'Can publish artwork'),
            publish_perm('Artwork.Meta', 'artwork')
        )

    def test_change_publish_perm_with_None(self):
        """
        publish_perm() raises a ValueError if the second parameter is None.
        """
        self.assertRaisesMessage(
            ValueError,
            'the parameter \'verbose_name\' may not be None or empty',
            publish_perm,
            'AppInformation.Meta',
            None
        )
