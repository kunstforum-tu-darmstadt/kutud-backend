from django.test import TestCase

from management.models import ArtworkMedia
from management.tests.models.test_artwork import set_up_bare_artwork, set_up_image


class ArtworkMediaModelTests(TestCase):
    """
    Tests for the database model ArtworkMedia and its functions.
    """

    def setUp(self) -> None:
        """
        Creates two images and two artworks.
        These objects are saved to the database.
        """
        self.image_one = set_up_image('Image 1', 'Unsplash')
        self.image_two = set_up_image('Image 2', 'Unsplash')
        self.artwork_one = set_up_bare_artwork('Art', self.image_one)
        self.artwork_two = set_up_bare_artwork('Artzzz', self.image_two)

    def test_next_index_with_no_media_assigned(self) -> None:
        """
        next_index() returns 1 if no media files are assigned to an artwork.
        """
        ArtworkMedia.objects.create(
            artwork=self.artwork_two,
            media=self.image_one,
            index=9999,
        )
        artwork_media = ArtworkMedia.objects.create(
            artwork=self.artwork_one,
            media=self.image_one,
        )
        self.assertEqual(artwork_media.index, 1)

    def test_next_index_with_media_assigned(self) -> None:
        """
        next_index() returns 2501 if a media files with the index 2500 is assigned to the artwork.
        """
        ArtworkMedia.objects.create(
            artwork=self.artwork_two,
            media=self.image_one,
            index=9999,
        )
        ArtworkMedia.objects.create(
            artwork=self.artwork_one,
            media=self.image_one,
            index=2500,
        )
        artwork_media = ArtworkMedia.objects.create(
            artwork=self.artwork_one,
            media=self.image_two,
        )
        self.assertEqual(artwork_media.index, 2501)
