from django.test import TestCase

from management.models import ExhibitionArtwork
from management.tests.util import set_up_bare_artwork, set_up_exhibition, set_up_image


class ExhibitionArtworkModelTests(TestCase):
    """
    Tests for the database model ExhibitionArtwork and its functions.
    """

    def setUp(self) -> None:
        """
        Creates two images, two artworks and two exhibitions.
        These objects are saved to the database.
        """
        self.image_one = set_up_image('Image 1', 'Unsplash')
        self.image_two = set_up_image('Image 2', 'Unsplash')
        self.artwork_one = set_up_bare_artwork('Art', self.image_one)
        self.artwork_two = set_up_bare_artwork('Artzzz', self.image_two)
        self.exhibition_one = set_up_exhibition(self.image_one)
        self.exhibition_two = set_up_exhibition(self.image_two)

    def test_next_index_with_no_artwork_assigned(self) -> None:
        """
        next_index() returns 1 if no artworks are assigned to an exhibition.
        """
        ExhibitionArtwork.objects.create(
            exhibition=self.exhibition_two,
            artwork=self.artwork_two,
            index=9999,
        )
        exhibition_artwork = ExhibitionArtwork.objects.create(
            exhibition=self.exhibition_one,
            artwork=self.artwork_one,
        )
        self.assertEqual(exhibition_artwork.index, 1)

    def test_next_index_with_artwork_assigned(self) -> None:
        """
        next_index() returns 2601 if an artwork with the index 2600 is assigned to the exhibition.
        """
        ExhibitionArtwork.objects.create(
            exhibition=self.exhibition_two,
            artwork=self.artwork_two,
            index=9999,
        )
        ExhibitionArtwork.objects.create(
            exhibition=self.exhibition_one,
            artwork=self.artwork_one,
            index=2600,
        )
        exhibition_artwork = ExhibitionArtwork.objects.create(
            exhibition=self.exhibition_one,
            artwork=self.artwork_two,
        )
        self.assertEqual(exhibition_artwork.index, 2601)
