from pathlib import Path

import humanize
from django.core.exceptions import ValidationError
from django.test import TestCase

from management.models.media import Media, media_upload_path, validate_file_size, validate_file_type


class MediaModelTests(TestCase):
    """
    Tests for the database model Media and its functions.
    """

    def test_validate_file_size_with_file_exceeding_maximum(self):
        """
        validate_file_size() raises a ValidationError is the size of the file is too large.
        """
        size = 2 * pow(2, 20) + 1
        maximum = 2 * pow(2, 20)
        self.assertRaisesMessage(
            ValidationError,
            'The size of the file ({}) is too large. The maximum file size is {}.'.format(
                humanize.naturalsize(size, binary=True, format="%.3f"),
                humanize.naturalsize(maximum, binary=True, format="%.3f")
            ),
            validate_file_size,
            size,
            maximum
        )

    def test_validate_file_size_with_file_below_maximum(self):
        """
        validate_file_size() doesn't raise any errors if the size of the file below the maximum size.
        """
        validate_file_size(2 * pow(2, 20) - 1, 2 * pow(2, 20))

    def test_validate_file_type_with_unexpected_type(self):
        """
        validate_file_type() raises a ValidationError if the content
        and the expected Internet media type of file don't match.
        """
        file_content = Path(__file__).with_name('test_media_file.mp3').open('rb').read(2024)
        expected_type = 'audio/mpeg'
        self.assertRaisesMessage(
            ValidationError,
            f'The Internet Media type of the file\'s content is image/jpeg '
            f'whereas the type determined by the file\'s extension is {expected_type}. '
            f'Please check if the extension of the file is correct.',
            validate_file_type,
            file_content,
            expected_type
        )

    def test_validate_file_type_with_valid_file(self):
        """
        validate_file_type() doesn't raise any errors if the expected and the actual type of the file match.
        """
        file_content = Path(__file__).with_name('test_media_file.jpg').open('rb').read(2024)
        validate_file_type(file_content, 'image/jpeg')

    def test_media_upload_path_1(self):
        """
        media_upload_path() returns the correct upload path for a file named 'image.png'.
        """
        media = Media()
        wanted = 'management/media/{0}.png'.format(media.uuid)
        got = media_upload_path(media, 'image.png')
        self.assertEqual(wanted, got)

    def test_media_upload_path_2(self):
        """
        media_upload_path() returns the correct upload path for a file named 'audio.mp3'.
        """
        media = Media()
        wanted = 'management/media/{0}.mp3'.format(media.uuid)
        got = media_upload_path(media, 'audio.mp3')
        self.assertEqual(wanted, got)
