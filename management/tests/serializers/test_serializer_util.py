from django.test import TestCase

from management.serializers.util import to_epoch_unix_timestamp, rename_field_serializations, \
    convert_time_fields_to_epoch, sort_filter_indexed_list_by_index


class SerializerUtilTests(TestCase):
    """
    Tests for the utility functions used in the serializers.
    """

    def setUp(self):
        """
        Creates several testing variables
        """
        self.date_time1 = "2020-03-22T13:17:27.853707"
        self.date_time2 = "1978-03-22T13:17:27.853707"
        self.date1 = "2020-03-22"
        self.date2 = "1978-03-22"
        self.model_serialization = {
            "key1": "value1",
            "key2": "value2",
            "key3": self.date_time1,
            "key4": self.date1
        }
        self.indexed_list = [
            (2, "two"),
            (1, "one"),
            (3, "three")
        ]

    def test_to_epoch_unix_timestamp_with_valid_time(self):
        """
        to_epoch_unix_timestamp() returns the right data and does not raise any errors
        with valid input
        """
        self.assertEqual(
            to_epoch_unix_timestamp(self.date_time1),
            1584879447
        )
        self.assertEqual(
            to_epoch_unix_timestamp(self.date_time2),
            259417047
        )
        self.assertEqual(
            to_epoch_unix_timestamp(self.date1),
            1584831600
        )
        self.assertEqual(
            to_epoch_unix_timestamp(self.date2),
            259369200
        )

    def test_to_epoch_unix_timestamp_with_invalid_time(self):
        """
        to_epoch_unix_timestamp() raises a ValueError when provided invalid data
        """
        self.assertRaises(
            ValueError,
            to_epoch_unix_timestamp,
            ""
        )
        self.assertRaises(
            ValueError,
            to_epoch_unix_timestamp,
            None
        )
        self.assertRaises(
            ValueError,
            to_epoch_unix_timestamp,
            " some random text"
        )

    def test_rename_field_serializations_with_valid_data(self):
        """
        rename_field_serializations() properly renames the keys in the dict
        """
        model_serialization_input = self.model_serialization
        model_serialization_expected = {
            "key_1": "value1",
            "key_2": "value2",
            "key3": self.date_time1,
            "key4": self.date1
        }

        self.assertEqual(
            rename_field_serializations(
                model_serialization_input,
                key_1="key1",
                key_2="key2"
            ),
            model_serialization_expected
        )

        self.assertEqual(
            self.model_serialization,
            self.model_serialization
        )

        self.assertEqual(
            rename_field_serializations(dict()),
            dict()
        )

    def test_rename_field_serializations_with_invalid_data(self):
        """
        rename_field_serializations() raises a ValueError when provided with invalid data
        """
        model_serialization_input = self.model_serialization

        self.assertRaises(
            ValueError,
            rename_field_serializations,
            None
        )

        self.assertRaises(
            KeyError,
            rename_field_serializations,
            model_serialization_input,
            key_5="key5"
        )

    def test_convert_time_fields_to_epoch_with_valid_data(self):
        """
        convert_time_fields_to_epoch() converts all specified fields to unix epochs without raising any errors
        """
        model_serialization_input = self.model_serialization
        model_serialization_expected_1 = {
            "key1": "value1",
            "key2": "value2",
            "key3": 1584879447,
            "key4": 1584831600
        }

        model_serialization_expected_2 = {
            "key1": "value1",
            "key2": "value2",
            "key3": self.date_time1,
            "key4": 1584831600
        }

        self.assertEqual(
            convert_time_fields_to_epoch(model_serialization_input, "key3", "key4"),
            model_serialization_expected_1
        )

        self.assertEqual(
            convert_time_fields_to_epoch(model_serialization_input, "key4"),
            model_serialization_expected_2
        )

        self.assertEqual(
            convert_time_fields_to_epoch(model_serialization_input),
            model_serialization_input
        )

        self.assertEqual(
            convert_time_fields_to_epoch(dict()),
            dict()
        )

    def test_convert_time_fields_to_epoch_with_invalid_data(self):
        """
        convert_time_fields_to_epoch() raises a ValueError when provided with invalid data
        """
        model_serialization_input = self.model_serialization

        self.assertRaises(
            ValueError,
            convert_time_fields_to_epoch,
            None
        )

        self.assertRaises(
            KeyError,
            convert_time_fields_to_epoch,
            model_serialization_input,
            "key5"
        )

    def test_sort_filter_indexed_list_by_index_with_valid_data(self):
        """
        sort_filter_indexed_list_by_index() returns a list only containing data, sorted by the provided indices
        without raising any errors
        """
        model_indexed_list = self.indexed_list

        self.assertEqual(
            sort_filter_indexed_list_by_index(model_indexed_list),
            ["one", "two", "three"]
        )

    def test_sort_filter_indexed_list_by_index_with_invalid_data(self):
        """
        sort_filter_indexed_list_by_index() raises a ValueError when provided with invalid data
        """
        self.assertRaises(
            ValueError,
            sort_filter_indexed_list_by_index,
            None
        )
