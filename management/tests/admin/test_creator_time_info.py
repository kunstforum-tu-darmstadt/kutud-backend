from django.contrib.auth.models import User
from django.http import HttpRequest
from django.test import TestCase
from django.test.client import RequestFactory

from management.admin.creator_time_info import internal_attributes_field_set, save_user
from management.models import Owner


def create_request_with_user() -> (User, HttpRequest):
    """
    Creates a user object and a request with the user and returns them both.

    See: https://stackoverflow.com/a/10277819/4106848
    See: https://stackoverflow.com/a/6321808/4106848

    :return: a tuple of the user and the request
    """
    user = User.objects.create_user(
        username='max', email='max@aol.com', password='very_secret'
    )
    factory = RequestFactory()
    request = factory.get('/')
    request.user = user
    return user, request


def create_owner() -> Owner:
    """
    Creates a new object of the database model Owner and persists it.

    :return: the newly created object
    """
    return Owner.objects.create(name='University')


class CreatorTimeInfoAdminTests(TestCase):

    def test_internal_attributes_field_set_with_id(self):
        """
        internal_attributes_field_set() adds the given parameter 'id' to the field list.
        """

        wanted = ('Internal attributes', {
            'classes': ('collapse',),
            'fields': ('id', 'editor', 'updated_at', 'updated_by', 'created_at', 'created_by')
        })
        got = internal_attributes_field_set('id')

        self.assertEqual(wanted, got)

    def test_internal_attributes_field_set_with_none(self):
        """
        internal_attributes_field_set() doesn't add any fields if the parameter is None.
        """

        wanted = ('Internal attributes', {
            'classes': ('collapse',),
            'fields': ('editor', 'updated_at', 'updated_by', 'created_at', 'created_by')
        })
        got = internal_attributes_field_set(None)

        self.assertEqual(wanted, got)

    def test_save_user_with_new_object(self):
        """
        save_user() should set the updated_by, created_by and editor fields of the object.
        """
        user, request = create_request_with_user()
        obj = create_owner()

        save_user(request, obj, False)

        self.assertEqual(obj.updated_by, user)
        self.assertEqual(obj.created_by, user)
        self.assertEqual(obj.editor, user)

    def test_save_user_with_update(self):
        """
        save_user() only should set the updated_by property of the object.
        """
        user, request = create_request_with_user()
        obj = create_owner()

        save_user(request, obj, True)

        self.assertEqual(obj.updated_by, user)
        self.assertEqual(obj.created_by, None)
        self.assertEqual(obj.editor, None)
