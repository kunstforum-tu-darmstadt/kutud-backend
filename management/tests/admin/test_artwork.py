from decimal import Decimal

from django.test import TestCase

from management.admin.artwork import google_maps_link, truncate_decimal_places


class ArtworkAdminTests(TestCase):

    def test_google_maps_link(self):
        """
        google_maps_link() returns the correct Google Maps URL for the given coordinates.
        """
        latitude = Decimal('49.8747233')
        longitude = Decimal('8.6539486')

        self.assertEqual(
            google_maps_link(latitude, longitude),
            '<a href="https://maps.google.com/maps?q=49.8747233,8.6539486" target="_blank">View on Google Maps</a>'
        )

    def test_google_maps_link_with_zero(self):
        """
        google_maps_link() returns the correct Google Maps URL for the given coordinates.
        """
        latitude = Decimal('0.0000001')
        longitude = Decimal('0.0000000')

        self.assertEqual(
            google_maps_link(latitude, longitude),
            '<a href="https://maps.google.com/maps?q=0.0000001,0.0000000" target="_blank">View on Google Maps</a>'
        )

    def test_truncate_decimal_places_with_equal(self):
        """
        truncate_decimal_places() returns the same number as before when 7 decimal places are passed.
        """
        decimal = Decimal('12.1234234')
        self.assertEqual(decimal, truncate_decimal_places(decimal))

    def test_truncate_decimal_places_with_more(self):
        """
        truncate_decimal_places() returns a decimal truncated to 7 decimal places.
        """
        decimal = Decimal('12.123423453636')
        self.assertEqual(Decimal('12.1234235'), truncate_decimal_places(decimal))

    def test_truncate_decimal_places_with_less(self):
        """
        truncate_decimal_places() returns a decimal with zero-padding up to 7 decimal places.
        """
        decimal = Decimal('12.123')
        self.assertEqual(Decimal('12.1230000'), truncate_decimal_places(decimal))
