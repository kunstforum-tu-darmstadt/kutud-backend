from django.test import TestCase

from management.admin.media import audio_tag, image_tag


class MediaAdminTests(TestCase):

    def test_image_tag(self):
        """
        image_tag() returns the correct <img> html tag for the given parameters.
        """
        url = 'https://example.com/image.png'
        height = 500

        wanted = '<img src="{}" height="{}" />'.format(url, height)
        got = image_tag(url, height)

        self.assertEqual(wanted, got)

    def test_audio_tag(self):
        """
        audio_tag() returns the correct <audio> html tag for the given parameters.
        """
        url = 'https://example.com/audio.mp3'
        media_type = 'audio/mpeg'

        wanted = '<audio controls><source src="{}" type="{}"></audio>'.format(url, media_type)
        got = audio_tag(url, media_type)

        self.assertEqual(wanted, got)
