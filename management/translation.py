from modeltranslation.translator import register, TranslationOptions
from .models.app_information import AppInformation
from .models.campus import Campus
from .models.exhibition import Exhibition
from .models.artist import Artist
from .models.artwork import Artwork


@register(AppInformation)
class AppInformationTranslationOptions(TranslationOptions):
    fields = ('text',)


@register(Campus)
class CampusTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(Exhibition)
class ExhibitionTranslationOptions(TranslationOptions):
    fields = ('title',)


@register(Artist)
class ArtistTranslationOptions(TranslationOptions):
    fields = ('info_text',)


@register(Artwork)
class ArtworkTranslationOptions(TranslationOptions):
    fields = ('name', 'type', 'canvas', 'acquired', 'info_text', 'location', 'linked_artwork_description',
              'audio_guide')
