from django.db import models

from management.models import CreatorTimeInfo
from management.models.permissions import change_edit_perm


class Artist(CreatorTimeInfo):
    """
    A database model containing information about artists.
    """

    # Data about the artist
    name = models.CharField(max_length=200, help_text='The full name of the artist (required)')
    year_birth = models.IntegerField(null=True, blank=True, help_text='The year the artist was born (optional)')
    year_death = models.IntegerField(null=True, blank=True, help_text='The year the artist died (optional)')
    info_text = models.TextField(
        null=True, blank=True, help_text='A text describing the artists life in short (optional)'
    )

    def __str__(self):
        return self.name

    class Meta(CreatorTimeInfo.Meta):
        ordering = ['name']
        permissions = [change_edit_perm(__qualname__, 'artist')]
