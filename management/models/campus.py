from django.db import models
from kunstforum import settings

from management.models import CreatorTimeInfo
from management.models.permissions import change_edit_perm
from modeltranslation.utils import build_localized_fieldname


class Campus(CreatorTimeInfo):
    """
    A database model containing information about campuses of the university.
    """

    # Data about the campus
    name = models.CharField(max_length=200, help_text='The name of the campus')
    prefix = models.CharField(max_length=2, help_text='Unique prefix of the campus for identification', default='')

    def __str__(self):
        language_code = settings.LANGUAGES[0][0]
        field_name = build_localized_fieldname('name', language_code)
        return getattr(self, field_name)

    class Meta(CreatorTimeInfo.Meta):
        verbose_name = 'campus'
        verbose_name_plural = 'campuses'
        ordering = ['prefix', 'name']
        permissions = [change_edit_perm(__qualname__, verbose_name)]
