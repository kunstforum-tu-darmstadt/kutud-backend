import mimetypes
import uuid
from os import path

import humanize
import magic
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from management.models import CreatorTimeInfo
from management.models.media_choices import CHOICE_AUDIO_GUIDE, CHOICE_IMAGE
from management.models.permissions import change_edit_perm


class Char32UUIDField(models.UUIDField):
    """
    A custom UUIDField to enabled backwards compatibility with Django 4.0's UUIDField.

    See: https://docs.djangoproject.com/en/5.0/releases/5.0/#migrating-uuidfield
    """
    def db_type(self, connection):
        return "char(32)"

    def get_db_prep_value(self, value, connection, prepared=False):
        value = super().get_db_prep_value(value, connection, prepared)
        if value is not None:
            value = value.hex
        return value

def validate_file_size(size: int, maximum_size: int) -> None:
    """
    Raises a ValidationError if the size is larger than the maximum_size.

    :param size: the actual size of the file
    :param maximum_size: the maximum allowed file size
    :raise ValidationError: if the size is larger than the maximum size
    """
    if size > maximum_size:
        raise ValidationError(
            _(
                'The size of the file (%(file_size)s) is too large. '
                'The maximum file size is %(maximum_size)s.'
            ),
            params={
                'file_size': humanize.naturalsize(size, binary=True, format="%.3f"),
                'maximum_size': humanize.naturalsize(maximum_size, binary=True, format="%.3f"),
            },
            code='file_size_too_large'
        )


def validate_file_type(file_content: bytes, expected_type: str) -> None:
    """
    Determines a file's Internet Media by readings its first 2048 bytes.
    If the determined type doesn't match the expected type, a ValidationError is raised.

    If the expected type is 'audio/mpeg',
    also the type 'application/octet-stream' is allowed due to the nature of mp3 files.

    :param file_content: the first 2048 bytes of the file to be checked
    :param expected_type: the expected Internet Media type of the file
    :raise ValidationError: if the determined type is not equal to the expected type
    """
    determined_type = magic.from_buffer(file_content, mime=True)

    # .mp3 files do not reval their type in all cases within their first 2048 bytes,
    # so we also accept files with an unknown type.
    # See: https://stackoverflow.com/a/2755288/4106848
    if expected_type == 'audio/mpeg' and determined_type == 'application/octet-stream':
        return

    if determined_type != expected_type:
        raise ValidationError(
            _(
                'The Internet Media type of the file\'s content is %(determined_type)s '
                'whereas the type determined by the file\'s extension is %(expected_type)s. '
                'Please check if the extension of the file is correct.'
            ),
            params={
                'determined_type': determined_type,
                'expected_type': expected_type
            },
            code='unexpected_file_mime_type'
        )


def media_upload_path(instance: 'Media', filename: str) -> str:
    """
    Generates a path to media upload media files based on their UUID and the extension of the uploader.

    :param instance: an instance of the media model
    :param filename: the name of the uploaded file
    :return: the path for the file
    """
    _, extension = path.splitext(filename)
    return 'management/media/{0}{1}'.format(instance.uuid, extension)


class Media(CreatorTimeInfo):
    """
    A database model describing user-uploaded media files.
    """

    CATEGORY_CHOICES = [
        (CHOICE_IMAGE, 'Image'),
        (CHOICE_AUDIO_GUIDE, 'Audio Guide')
    ]

    # Identifier
    uuid = Char32UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False, verbose_name='UUID',
        help_text='A randomly generated UUID for identifying uploaded media files'
    )
    # Information supplied by the uploader
    description = models.CharField(
        max_length=2000,
        help_text='A concise textual description of the media file. '
                  'It is used to choose media files for other objects'
    )
    copyright = models.CharField(
        max_length=2000,
        help_text='Copyright notice for the media file'
    )
    copyright_url = models.CharField(
        null=True, blank=True,
        max_length=200,
        help_text='Website URL of the person owning the copyright of this media file.'
    )
    category = models.CharField(
        max_length=100, choices=CATEGORY_CHOICES,
        help_text='The category of the media file. It determines what files are allowed and where they can be used'
    )

    # Information about the file
    file = models.FileField(
        upload_to=media_upload_path,
        help_text='The file itself. '
                  'You can upload a new file or edit the current file using the button.<br/> '
                  'Files of the category image may not exceed 2MB in size and '
                  'only the file extensions png, jpg & jpeg are allowed.<br/> '
                  'Files of the category audio guide may not exceed 5MB in size and '
                  'only the file extension mp3 is allowed.'
    )
    type = models.CharField(
        max_length=200, editable=False,
        help_text='The <a href="https://de.wikipedia.org/wiki/Internet_Media_Type">Internet Media Type</a> of the file'
    )

    __original_file = None

    def __init__(self, *args, **kwargs):
        # Detect if a media file was changed
        # https://stackoverflow.com/a/1793323
        super().__init__(*args, **kwargs)
        self.__original_file = self.file

    # Cross field validation
    def clean(self):
        super().clean()

        # Only validate if a file was uploaded
        if self.file:
            # Determines a file Internet media type using its extension
            self.type = mimetypes.guess_type(self.file.name)[0]

            # Read the first 2048 bytes from the file in binary mode
            # https://docs.python.org/3/library/functions.html#open
            file_content = self.file.open("rb").read(2048)
            validate_file_type(file_content, self.type)

            if self.category == CHOICE_IMAGE:
                FileExtensionValidator(['png', 'jpg', 'jpeg'])(self.file)
                validate_file_size(self.file.size, 2 * pow(2, 20))  # Max: 2 MiB
            else:
                FileExtensionValidator(['mp3'])(self.file)
                validate_file_size(self.file.size, 5 * pow(2, 20))  # Max: 5 MiB

    def save(self, *args, **kwargs):
        if self.file and (self.file != self.__original_file):
            # The storage provider to support different backends
            storage = self.file.storage
            # The path that the file should be saved to (UUID + extension)
            desired_path = media_upload_path(self, self.file.name)

            file_extensions = ('.jpg', '.jpeg', '.mp3', '.png')
            desired_name, _ = path.splitext(desired_path)

            # If a file with the desired name already exists in the file system, regardless of its extension, delete it
            # This is done to prevent cluttering of the file system
            for extension in file_extensions:
                if storage.exists(desired_name + extension):
                    storage.delete(desired_name + extension)

        super().save(*args, **kwargs)
        self.__original_file = self.file

    def __str__(self):
        return "{}: {}".format(self.get_category_display(), self.description)

    class Meta(CreatorTimeInfo.Meta):
        verbose_name = 'media'
        verbose_name_plural = 'media'
        ordering = ['-updated_at']
        indexes = [
            models.Index(fields=['updated_at'])
        ]
        permissions = [change_edit_perm(__qualname__, verbose_name)]
