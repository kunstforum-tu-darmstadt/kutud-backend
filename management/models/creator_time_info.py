from django.conf import settings
from django.db import models


class CreatorTimeInfo(models.Model):
    """
    An abstract database model holding information about when and by whom an object was created and updated.
    The model also stores which user is responsible the object.
    """

    # Users
    # The setting related_name='+' implies that the User model won't have a backwards relation to the property
    # https://docs.djangoproject.com/en/4.0/ref/models/fields/#django.db.models.ForeignKey.related_name
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, editable=False, related_name='+',
        help_text='The user who created the object'
    )
    updated_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, editable=False, related_name='+',
        help_text='The user who last updated the object'
    )
    editor = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True,
        help_text='The user who is currently maintaining the object'
    )

    # Timestamps of creating & update
    created_at = models.DateTimeField(auto_now_add=True, help_text='Time and date of creation')
    updated_at = models.DateTimeField(auto_now=True, help_text='Time and date of the last update')

    class Meta:
        abstract = True
