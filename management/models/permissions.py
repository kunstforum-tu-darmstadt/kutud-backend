# Django prevents defining abstract custom permissions in Meta models.
# Therefore, we had to build our own solution.
# See https://code.djangoproject.com/ticket/10686

def extract_model_name(qualified_name: str) -> str:
    """
    Extracts the model name from the qualified name of Meta classes.

    :param qualified_name: the __qualname__ of the Meta class
    :return: the model name
    """
    if not qualified_name:
        raise ValueError('the parameter \'qualified_name\' may not be None or empty')
    return qualified_name.split('.', 1)[0].lower()


def change_edit_perm(qualified_name: str, verbose_name: str) -> tuple:
    """
    Returns the permission 'change_editor_%(model_name)' for the database model.

    :param qualified_name: the __qualname__ of the Meta class
    :param verbose_name: the verbose name of the model in singular form
    :return: the permission tuple
    """
    if not verbose_name:
        raise ValueError('the parameter \'verbose_name\' may not be None or empty')
    model_name = extract_model_name(qualified_name)
    return f'change_editor_{model_name}', f'Can change {verbose_name} if assigned editor'


def publish_perm(qualified_name: str, verbose_name: str) -> tuple:
    """
    Returns the permission 'publish_%(model_name)' for the database model.

    :param qualified_name: the __qualname__ of the Meta class
    :param verbose_name: the verbose name of the model in singular form
    :return: the permission tuple
    """
    if not verbose_name:
        raise ValueError('the parameter \'verbose_name\' may not be None or empty')
    model_name = extract_model_name(qualified_name)
    return f'publish_{model_name}', f'Can publish {verbose_name}'
