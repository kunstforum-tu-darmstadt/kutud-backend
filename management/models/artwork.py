import re
import hashlib
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from management.models import CreatorTimeInfo
from management.models.media_choices import LC_AUDIO, LC_IMAGE
from management.models.permissions import change_edit_perm, publish_perm


def validate_no_self_reference(artwork: 'Artwork') -> None:
    """
    Checks if the field linked_artwork references to the artwork itself.
    If yes, a ValidationError is raised.

    :param artwork: an instance of the Artwork model
    :raise ValidationError: if the field linked_artwork references the artwork itself
    """
    if artwork.linked_artwork is not None and artwork == artwork.linked_artwork:
        raise ValidationError(
            _('The field \'linked artwork\' may not link to the artwork itself'),
            code='linked_artwork_self_reference'
        )


def validate_youtube_url(url: str) -> None:
    """
    Checks if the given URL is a YouTube-video-URL.

    :param url: a URL entered by a user
    :raise ValidationError: if the URL doesn't link directly to a YouTube video
    """
    short_url = 'https://youtu.be/'
    long_url = 'https://www.youtube.com/watch?v='
    if not url.startswith(short_url) and not url.startswith(long_url):
        raise ValidationError(
            _('The given URL does not link to a YouTube video. '
              'The URL has to start with https://www.youtube.com/watch?v= or https://youtu.be/'),
            code='not_youtube_video_url'
        )


def validate_lat_long_set(latitude: Decimal | None, longitude: Decimal | None) -> None:
    """
    Checks that if either latitude or longitude is set, the
    respective other field is set as well.

    :param latitude: the latitude of the artwork
    :param longitude: the longitude of the artwork
    :raise ValidationError: if one of the fields was set but the other was not
    """
    if latitude is not None and longitude is None or latitude is None and longitude is not None:
        raise ValidationError(
            _('If either latitude or longitude is set, the corresponding other field has to be set as well'),
            code='latitude_longitude_not_correctly_set'
        )


def normalize_youtube_url(url: str) -> str:
    """
    Normalizes a short youtu.be URL to the longer youtube.com format.
    If the parameter url isn't a short youtu.be URL it is returned as is.

    :param url: the URL to normalize
    :return: the normalized URL
    """
    short = 'https://youtu.be/'
    long = 'https://www.youtube.com/watch?v='
    if re.match(short, url, re.I):
        # The video id can additionally contain a timestamp for the start of the video
        video_id = url[len(short):].replace('?', '&', 1)
        return '{}{}'.format(long, video_id)
    else:
        return url


class Artwork(CreatorTimeInfo):
    """
    A database model describing an artwork with its data and images belonging to it.
    """

    # Additional identifier
    identifier = models.CharField(
        # To allow blank values and require otherwise unique values, null=True is required
        unique=True, max_length=6, blank=True, null=True,
        help_text='A unique identifier for the artwork (will be generated)',
    )

    # Basic data about the artwork
    name = models.CharField(
        max_length=200,
        help_text='The name / title of the artwork (required)'
    )
    published = models.BooleanField(
        default=False,
        help_text='Choose whether the artwork is shown in the app. '
                  'To publish an artwork, it\'s necessary to fill all fields and add at least one image'
    )

    # More in-depth data of the artwork
    year = models.CharField(
        max_length=200, blank=True,
        help_text='The year the artwork was created (optional)'
    )
    type = models.CharField(
        max_length=200, blank=True,
        help_text='The category the artwork belongs to (e.g. painting or outdoors sculpture) (optional)'
    )
    canvas = models.CharField(
        max_length=200, blank=True,
        help_text='The canvas or material that was used to create the artwork (optional)'
    )
    artist = models.ForeignKey(
        'Artist', null=True, blank=True, on_delete=models.PROTECT,
        help_text='The artist who created the artwork (optional)'
    )
    owner = models.ForeignKey(
        'Owner', null=True, blank=True, on_delete=models.PROTECT,
        help_text='The person or entity which currently owns the artwork (optional)'
    )
    acquired = models.CharField(
        max_length=2000, blank=True,
        help_text='A text describing in short how the artwork was acquired (optional)'
    )
    info_text = models.TextField(
        blank=True,
        help_text='A text containing additional information about the artwork (required for publishing)'
    )
    linked_artwork = models.ForeignKey(
        'Artwork', null=True, blank=True, on_delete=models.PROTECT,
        related_name='+',
        help_text='A referenced artwork from another exhibition (optional)<br>'
                  'Click the lookup button to search existing artworks.<br>'
                  'The ID will be set automatically after choosing an artwork.'
    )
    linked_artwork_description = models.TextField(
        blank=True, null=True,
        help_text='Description for a referenced artwork from another exhibition (optional)<br>'
                  'Requires a linked artwork to be set.'
    )

    # Media
    images = models.ManyToManyField(
        'Media', through='ArtworkMedia', related_name='artworks',
        help_text='Images of the artwork (at least one required for publishing)'
    )
    cover_image = models.ForeignKey(
        'Media', null=True, blank=True, on_delete=models.PROTECT, limit_choices_to=LC_IMAGE,
        related_name='artwork_cover_image',
        help_text='An image used as the cover for the artwork (required for publishing)'
    )
    audio_guide = models.ForeignKey(
        'Media', null=True, blank=True, on_delete=models.PROTECT, limit_choices_to=LC_AUDIO,
        related_name='artwork_audio_guide',
        help_text='Audio explaining details about the artwork (optional)'
    )
    video_link = models.URLField(
        max_length=200, null=True, blank=True, validators=[validate_youtube_url],
        help_text='A URL of a YouTube video about the artwork. '
                  'It has to begin with \'https://www.youtube.com/watch?v=\' (optional)'
    )

    # Location
    # Uses seven decimal places for accuracy as it's the practical limit of commercial surveying (max 11.1mm)
    # See: https://en.wikipedia.org/wiki/Decimal_degrees
    # 2 non-decimal digits for the latitude as its value is bounded by +-90°
    latitude = models.DecimalField(
        max_digits=2 + 7, decimal_places=7, null=True, blank=True,
        help_text='The latitude geographic coordinate given in the '
                  '<a href="https://en.wikipedia.org/wiki/Decimal_degrees">decimal degrees</a> format (optional)'
    )
    # 3 non-decimal digits for the longitude as its value is bounded by +-180°
    longitude = models.DecimalField(
        max_digits=3 + 7, decimal_places=7, null=True, blank=True,
        help_text='The longitude geographic coordinate given in the '
                  '<a href="https://en.wikipedia.org/wiki/Decimal_degrees">decimal degrees</a> format (optional)'
    )
    location = models.CharField(
        max_length=200, null=True, blank=True,
        help_text='A precise text description where to find the artwork on campus '
                  '(e.g. S2|02 Robert Piloty Gebäude, Außengelände) (optional)'
    )
    address = models.CharField(
        max_length=200, null=True, blank=True,
        help_text='The post address of a building near the artwork (e.g. Hochschulstraße 10) (optional)'
    )
    campus = models.ForeignKey(
        'Campus', null=True, blank=True, on_delete=models.PROTECT,
        help_text='The campus where the artwork is located (required)',
    )

    def generate_id(self, name, campus_id):
        hash_indices = [0, 2, 4, 6]

        def build_hash(_id, _name):
            return hashlib.md5((_id + _name).encode()).hexdigest().upper()

        hash = build_hash(campus_id, name)
        built_id = campus_id + hash[hash_indices[0]] + hash[hash_indices[1]] + hash[hash_indices[2]] + hash[
            hash_indices[3]]

        while (len(Artwork.objects.filter(identifier__exact=built_id))) > 0 \
                and Artwork.objects.get(pk=self.pk).identifier != built_id:
            for i in range(len(hash_indices)):
                hash_indices[i] = hash_indices[i] + 1
            built_id = campus_id + hash[hash_indices[0]] + hash[hash_indices[1]] + hash[hash_indices[2]] + hash[
                hash_indices[3]]
        return built_id

    def clean(self):
        super().clean()

        validate_no_self_reference(self)
        validate_lat_long_set(self.latitude, self.longitude)

        if self.video_link is not None:
            self.video_link = normalize_youtube_url(self.video_link)

    def __str__(self):
        if self.artist is None:
            return self.name
        else:
            return '{0} - {1} ({2})'.format(self.identifier, self.name, self.artist.name)

    class Meta(CreatorTimeInfo.Meta):
        permissions = [change_edit_perm(__qualname__, 'artwork'), publish_perm(__qualname__, 'artwork')]
