from django.db import models


class ExportSupport(models):
    """
    A database model solely for purpose of creating the permission 'export_data'.

    https://stackoverflow.com/a/37988537
    """

    class Meta:
        # Don't create a database table for this model
        managed = False
        # Don't create default permissions for this model
        default_permissions = []
        #
        permissions = [
            ('export_data', 'Export all data in human-readable format')
        ]
