import datetime

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from management.models import CreatorTimeInfo
from management.models.media_choices import LC_IMAGE
from management.models.permissions import change_edit_perm, publish_perm


def validate_date_not_earlier_than_other(start_date: datetime.date, end_date: datetime.date) -> None:
    """
    Validates that the start date is equal to or later than the end date.
    If not a ValidationError is raised.

    :param start_date: the date an exhibition starts
    :param end_date: the date an exhibition ends
    :raise ValidationError: if the end date is earlier than the start date
    """
    if start_date is not None and end_date is not None:
        if end_date < start_date:
            raise ValidationError(
                _('The end date (%(end_date)s) is earlier than the start date (%(start_date)s)'),
                params={'start_date': start_date, 'end_date': end_date},
                code='end_date_earlier_than_start_date'
            )


def validate_artworks_set(artworks: models.QuerySet) -> None:
    """
    Checks that at least one artwork is added to exhibition.
    If not, a ValidationError is raised.

    :param artworks: an instance of the field artworks of the exhibition model
    :raise ValidationError: if no artwork was added to the exhibition
    """
    if artworks.count() < 1:
        raise ValidationError(
            _('Please add at least one artwork to the exhibition in order to publish it'),
            code='no_exhibition_artworks_set'
        )


class Exhibition(CreatorTimeInfo):
    """
    A database model describing an exhibition with its data and artworks belonging to it.
    """

    # Data about the exhibition
    title = models.CharField(max_length=200, help_text='The name / title of the exhibition')
    start_date = models.DateField(help_text='The date when the exhibition starts')
    end_date = models.DateField(help_text='The date when the exhibition ends')
    published = models.BooleanField(
        default=False,
        help_text='To publish an exhibition it\'s necessary to publish each underlying artwork'
    )
    published_exception = models.BooleanField(
        default=False,
        help_text='Add publication status exception (unpublished artworks included in exhibition)'
    )

    # Media
    cover_image = models.ForeignKey(
        'Media', on_delete=models.PROTECT, limit_choices_to=LC_IMAGE,
        help_text='An image used as the cover for the exhibition'
    )
    artworks = models.ManyToManyField(
        'Artwork', through='ExhibitionArtwork',
        help_text='The artworks displayed in the exhibition'
    )

    def update_published_status(self):
        published_count = 0
        for artwork in self.artworks.all():
            if artwork.published:
                published_count += 1
        self.published = (published_count > 0 and
                          published_count == self.artworks.count()) or self.published_exception is True

    def clean(self):
        super().clean()
        # Validate that the end date is not earlier than the start date
        validate_date_not_earlier_than_other(self.start_date, self.end_date)

    def __str__(self):
        return self.title

    class Meta(CreatorTimeInfo.Meta):
        permissions = [change_edit_perm(__qualname__, 'exhibition'), publish_perm(__qualname__, 'exhibition')]
