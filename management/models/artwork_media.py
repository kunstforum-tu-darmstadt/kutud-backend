from django.db import models

from management.models.media_choices import LC_IMAGE


def next_index(self: 'ArtworkMedia') -> int:
    """
    Gets the largest index of artwork media entries for the artwork, adds one and returns it.
    :return: the largest index for the artwork plus 1
    """
    try:
        artwork_media = ArtworkMedia.objects.filter(artwork=self.artwork).latest('index')
        return artwork_media.index + 1
    except ArtworkMedia.DoesNotExist:
        return 1


class ArtworkMedia(models.Model):
    """
    A database model describing the relationship between artworks and media.
    The additional field 'index' allows ordering media for artworks.
    """

    artwork = models.ForeignKey(
        'Artwork', on_delete=models.CASCADE,
        help_text='The artwork the image belongs to'
    )
    media = models.ForeignKey(
        'Media', on_delete=models.CASCADE, limit_choices_to=LC_IMAGE,
        help_text='An image of the artwork'
    )
    index = models.PositiveIntegerField(
        blank=True,
        help_text='A positive integer used to order images for an artwork. No two images may have the same index'
    )

    def save(self, *args, **kwargs):
        if self.index is None:
            self.index = next_index(self)

        super().save(*args, **kwargs)

    def __str__(self):
        return "{} + {}".format(self.artwork, self.media)

    class Meta:
        # Defines a constraint which enforces that
        # - a media may only be used once per artwork
        # - values for the field index only be used once per artwork
        # - at maximum
        # https://docs.djangoproject.com/en/4.0/ref/models/constraints/#django.db.models.UniqueConstraint
        constraints = [
            models.UniqueConstraint(fields=['artwork', 'media'], name='unique_artwork_media'),
            models.UniqueConstraint(fields=['artwork', 'index'], name='unique_artwork_index'),
        ]
        verbose_name = 'artwork media'
        verbose_name_plural = 'artwork media'
