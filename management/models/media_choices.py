# Choices for the field 'category' of the model 'Media'
CHOICE_IMAGE = 'image'
CHOICE_AUDIO_GUIDE = 'audio-guide'

# Dictionaries to limit choices for foreign keys
# https://docs.djangoproject.com/en/4.0/ref/models/fields/#django.db.models.ForeignKey.limit_choices_to
LC_IMAGE = {'category': CHOICE_IMAGE}
LC_AUDIO = {'category': CHOICE_AUDIO_GUIDE}
