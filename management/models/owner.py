from django.db import models

from management.models import CreatorTimeInfo
from management.models.permissions import change_edit_perm


class Owner(CreatorTimeInfo):
    """
    A database model describing persons or entities which own artworks.
    """

    # Data about the owner
    name = models.CharField(max_length=200, help_text='The full name of a person or entity owning artworks')

    def __str__(self):
        return self.name

    class Meta(CreatorTimeInfo.Meta):
        permissions = [change_edit_perm(__qualname__, 'owner')]
