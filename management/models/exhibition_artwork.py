from django.db import models


def next_index(self: 'ExhibitionArtwork') -> int:
    """
    Gets the largest index of exhibition artwork entries for the exhibition, adds one and returns it.
    :return: the largest index for the exhibition plus 1
    """
    try:
        exhibition_artwork = ExhibitionArtwork.objects.filter(exhibition=self.exhibition).latest('index')
        return exhibition_artwork.index + 1
    except ExhibitionArtwork.DoesNotExist:
        return 1


class ExhibitionArtwork(models.Model):
    """
    A database model describing the relationship between exhibitions and artworks.
    The additional field 'index' allows ordering artworks for exhibitions.
    """

    exhibition = models.ForeignKey(
        'Exhibition', on_delete=models.CASCADE,
        help_text='The exhibition the artwork belongs to'
    )
    artwork = models.ForeignKey(
        'Artwork', on_delete=models.CASCADE,
        help_text='An artwork belonging to the exhibition'
    )
    index = models.PositiveIntegerField(
        blank=True,
        help_text='A positive integer used to order artworks for an exhibition. No two artworks may have the same index'
    )

    def save(self, *args, **kwargs):
        if self.index is None:
            self.index = next_index(self)

        super().save(*args, **kwargs)

    def __str__(self):
        return "{} + {}".format(self.exhibition, self.artwork)

    class Meta:
        # Defines a constraint which enforces that the field index just can be used once per artwork
        # https://docs.djangoproject.com/en/4.0/ref/models/constraints/#django.db.models.UniqueConstraint
        constraints = [
            models.UniqueConstraint(fields=['exhibition', 'artwork'], name='unique_exhibition_artwork'),
            models.UniqueConstraint(fields=['exhibition', 'index'], name='unique_exhibition_index'),
        ]
