from django.db import models

from management.models import CreatorTimeInfo
from management.models.permissions import change_edit_perm
from management.models.app_information_choices import CHOICE_APP_WEBSITE, CHOICE_ART_AT_UNIVERSITY, CHOICE_GROUP, \
    CHOICE_IMPRINT, CHOICE_DATA_PROTECTION


class AppInformation(CreatorTimeInfo):
    """
    A database model describing various types of informational texts and URLs.
    """

    KEY_CHOICES = [
        (CHOICE_GROUP, 'Information about our group'),
        (CHOICE_ART_AT_UNIVERSITY, 'Information about art at the university'),
        (CHOICE_IMPRINT, 'Imprint'),
        (CHOICE_APP_WEBSITE, 'Website for the app'),
        (CHOICE_DATA_PROTECTION, 'The data protection declaration'),
    ]

    # Information
    key = models.CharField(
        max_length=200, choices=KEY_CHOICES, primary_key=True,
        help_text='The type of information'
    )
    text = models.TextField(
        help_text='The informative text or an URL.<br>'
                  'The URL for the website of the app must end with a seperator.<br>'
                  'The seperator \'#\' allows websites to ignore the artwork identifiers appended to their URL.<br>'
                  'The separators \'=\' and \'/\' allow websites to process '
                  'the artwork identifiers appended to their URL.'
    )

    def __str__(self):
        return self.key

    class Meta(CreatorTimeInfo.Meta):
        verbose_name = 'app information'
        verbose_name_plural = 'app information'
        permissions = [change_edit_perm(__qualname__, verbose_name)]
