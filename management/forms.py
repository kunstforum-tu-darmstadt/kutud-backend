from django import forms
from django.contrib.auth.models import User
from django.db.models import QuerySet


class ChangeEditorForm(forms.Form):
    """
    A form used to assign a new editor to serval objects.
    """

    # Classes to style the form
    # See: https://docs.djangoproject.com/en/4.0/ref/forms/api/#styling-required-or-erroneous-form-rows
    required_css_class = 'required'
    error_css_class = 'error'

    editor = forms.ModelChoiceField(
        queryset=User.objects.all(),
        required=True,
        help_text='Select the user which should be assigned editor of the list objects',
    )

    def __init__(self, objects: QuerySet, *args, **kwargs):
        self.objects = objects
        super().__init__(*args, **kwargs)

    def save(self) -> tuple[int, User]:
        """
        Sets the editor for all objects in the given queryset.
        Returns the number of updated objects and the selected editor.

        :return: the number of updated objects, the selected editor
        """
        editor = self.cleaned_data["editor"]
        updated = self.objects.update(editor=editor)
        return updated, editor
