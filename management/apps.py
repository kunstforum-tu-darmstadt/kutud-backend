from django.apps import AppConfig


class ManagementConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'management'

    # Initialize signal connections
    def ready(self):
        # Implicit signal connecting due to receiver decorator
        # https://docs.djangoproject.com/en/4.0/topics/signals/#connecting-receiver-functions-1
        # The 'noqa' comment informs linters to ignore the unused import by skipping the line entirely
        import management.signals # noqa
