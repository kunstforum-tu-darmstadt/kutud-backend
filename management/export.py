import decimal
import os
import shutil
import tempfile
import zipfile
from typing import Callable
from uuid import UUID

import orjson as json  # https://github.com/ijl/orjson
from django.http import HttpResponse
from django.utils.text import slugify

from kunstforum.settings import MEDIA_ROOT
from management.admin import AppInformationResource, ArtistResource, ArtworkResource, CampusResource, \
    ExhibitionResource, MediaResource, OwnerResource

"""
For copying and writing (especially with regard to media files), tools that assume the use of a classic filesystem
bases storage are utilized. For more sophisticated storage systems like S3, a more complex version of this code
using custom django storage backends can be utilized. At the time of creation, the necessary workload was not be
justified by the possible advantages ('mit Kanonen auf Spatzen schiessen'), which is why those cases were omitted.
"""

TARGETED_APP_NAME = 'management'

exported_media_data = MediaResource().export().dict


def get_zipped_data(response_object: HttpResponse) -> None:
    """
    Exports all interesting content stored in the database and on disk in a directory structure as a zip file.
    The zip file is copied directly into an HttpResponse object, so that it can be easily served in the browser.

    :param response_object: the HttpResponse object the zip file is copied into
    """

    tlds = {
        'app_information': AppInformationResource().export().dict,
        'artists': ArtistResource().export().dict,
        'artworks': ArtworkResource().export().dict,
        'campuses': CampusResource().export().dict,
        'exhibitions': ExhibitionResource().export().dict,
        'owners': OwnerResource().export().dict,
    }

    # Creates an anonymous (-> obfuscated), temporary directory in the OS's respective temporary directory
    # (e.g. /tmp on Linux). As soon as it is not needed any longer (-> when we exit the context manager's context),
    # the directory will be removed from disk.
    # https://docs.python.org/3/library/tempfile.html#tempfile.TemporaryDirectory
    with tempfile.TemporaryDirectory() as export_directory:
        for dir_name, content in tlds.items():
            content_path = os.path.join(export_directory, dir_name)
            os.mkdir(content_path)
            _create_dirs_from_dict(dir_name, content_path, content)

        # Inspired by https://stackoverflow.com/a/1855118
        with zipfile.ZipFile(response_object, 'w', zipfile.ZIP_DEFLATED) as zipf:
            _zipdir(export_directory, zipf)


def _zipdir(path: str, ziph: zipfile.ZipFile) -> None:
    """
    Adapted from https://stackoverflow.com/a/1855118
    Creates a directory structure in a given zipfile based on the provided path.

    :param path: the path to the to-be-zipped top-level-directory (tld)
    :param ziph: a reference to the zip file the structure is to-be-written into
    """
    subdirs = [os.path.join(path, subpath) for subpath in os.listdir(path)]

    for subdir in subdirs:
        for root, dirs, files in os.walk(subdir):
            for file in files:
                ziph.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(subdir, '..')))


def _create_dirs_from_dict(content_type: str, path: str, content: list) -> None:
    """
    Creates a directory structure based on the type of data that is provided.

    :param content_type: the type of content the structure is to-be-created for (e.g. 'exhibitions' or 'artists')
    :param path: the path to the tld for the given content
    :param content: the actual content, retrieved from the database
    """
    if content_type == 'app_information':
        _create_subdirs(path, content, 'key')
    elif content_type == 'exhibitions':
        _create_subdirs(path, content, 'title', use_id=True, extra_dir_structure=_exhibition_media_structure)
    elif content_type == 'artworks':
        _create_subdirs(path, content, 'name', use_id=True, extra_dir_structure=_artwork_media_structure)
    else:
        _create_subdirs(path, content, 'name', use_id=True)


def _create_subdirs(
        path: str,
        content: list,
        naming_key: str,
        use_id: bool = False,
        extra_dir_structure: Callable = None
) -> None:
    """
    Creates and fills subdirectories with more subdirectories or files, given their definition in the database
    and thus their content.

    :param path: the path to the tld for the given content
    :param content: the actual content, retrieved from the database
    :param naming_key: the key based on which the directories/files should be named
    :param use_id: if the content's id should additionally be used to name the directories/files due to ambiguities
    :param extra_dir_structure: a function that creates more structures if necessary
    """
    for entry in content:
        slugified_value_of_entry_key = slugify(entry[naming_key])
        if use_id:
            ident = f'{entry["id"]}-{slugified_value_of_entry_key}'
        else:
            ident = slugified_value_of_entry_key

        content_entry_path = os.path.join(path, ident)
        os.mkdir(content_entry_path)

        _write_json_to_file(content_entry_path, f'{ident}.json', entry)

        if extra_dir_structure is not None:
            extra_dir_structure(entry, content_entry_path)


def default(obj):
    """
    https://github.com/ijl/orjson#default

    :param obj: the object that is to be treated
    :raise TypeError: if the object is not of type Decimal
    """
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError


def _exhibition_media_structure(db_entry: dict, path: str) -> None:
    """
    Creates a structure of directories and files for an exhibition.

    :param db_entry: the exhibition for which the structure is to-be-created
    :param path: the tld in which the structure should be created
    """
    cover_image_uuid = db_entry['cover_image']

    _create_media_dir(cover_image_uuid, path)


def _artwork_media_structure(db_entry: dict, path: str) -> None:
    """
    Creates a structure of directories and files for an artwork.

    :param db_entry: the artwork for which the structure is to-be-created
    :param path: the tld in which the structure should be created
    """
    cover_image_uuid = db_entry['cover_image']
    artwork_images_uuids = db_entry['images'].split(',')
    audio_guide_uuid = db_entry['audio_guide']

    _create_media_dir(cover_image_uuid, path)
    _create_media_dir(audio_guide_uuid, path)

    for image_uuid in artwork_images_uuids:
        _create_media_dir(image_uuid, path)


def _create_media_dir(uuid: UUID | str, path: str) -> None:
    """
    Creates and fills a media directory with both the actual media files and the corresponding metadata.

    :param uuid: the media's uuid for which the structure should be created
    :param path: the tld in which the structure should be created
    """
    str_uuid = str(uuid)

    media_dir = os.path.join(path, str_uuid)
    if not os.path.isdir(media_dir):
        # Only continue if the folder not already exists
        # (e.g. because the cover image is also used as an artwork image)
        os.mkdir(media_dir)

        media_uuid_list = [media_meta_dict['uuid'] for media_meta_dict in exported_media_data]
        media_dict = exported_media_data[media_uuid_list.index(str_uuid)]

        _write_json_to_file(media_dir, f'{str_uuid}.json', media_dict)
        _copy_media_file(str_uuid, media_dir)


def _copy_media_file(source_uuid: str, target_dir: str) -> None:
    """
    Copies a media file from its media folder in MEDIA_ROOT to the targeted directory given its uuid.

    :param source_uuid: the uuid of the media file that is to-be-copied
    :param target_dir: the targeted directory to which the media file should be copied to
    """
    source_dir = os.path.join(MEDIA_ROOT, TARGETED_APP_NAME, 'media')
    media_files = os.listdir(source_dir)

    # Size will at max be 1 since uuids are unique
    source_file_name_list = [media_file for media_file in media_files if source_uuid in media_file]

    if len(source_file_name_list) == 0:
        return

    source_file_name = source_file_name_list[0]
    source_file_path = os.path.join(source_dir, source_file_name)

    shutil.copy(source_file_path, target_dir)


def _write_json_to_file(path: str, filename: str, data: dict) -> None:
    """
    Writes a prettified json file to the specified directory.

    :param path: the targeted path
    :param filename: the desired filename for the json file (should end on .json)
    :param data: the data with which the json file should be filled
    """
    with open(os.path.join(path, filename), 'w', encoding='utf-8') as outfile:
        outfile.seek(0)  # Set data pointer to the beginning of the file
        # json.OPT_INDENT_2 pretty prints with two spaces as indentation
        # https://github.com/ijl/orjson#opt_indent_2
        outfile.write(json.dumps(data, default=default, option=json.OPT_INDENT_2).decode('utf-8'))
