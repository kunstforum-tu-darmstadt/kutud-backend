from django.db.models import Prefetch
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import status
from rest_framework.decorators import api_view
from django.db.models import Q
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from django.http import JsonResponse
from datetime import datetime, timedelta

from management.models import AppInformation, Artist, Artwork, Exhibition, ExhibitionArtwork, Media
from management.pagination import APISpecificResultsSetPagination
from management.serializers import AppInformationSerializer, ArtistSerializer, \
    ArtworkSerializer, ExhibitionSerializer, MediaSerializer, MediaCopyrightSerializer

CACHE_TTL = 60 * 60 * 12  # 12 hours


def set_admin_language(request):
    if request.method == 'POST':
        language_code = request.POST.get('language')

        # Store the language preference in the session
        request.session['language'] = language_code

        # Alternatively, store the language preference in a cookie
        response = JsonResponse({'status': 'success'})
        # Set Cookie Duration here
        expires = datetime.utcnow() + timedelta(days=7)
        response.set_cookie('language', language_code, expires=expires)
        return response
    else:
        return JsonResponse({'status': 'error'})


@api_view(['GET'])
def api_status(request: Request) -> Response:
    """
    Serves the API status (always online for now) as JSON

    :param request: the request on this route
    :return: the response JSON containing status information
    """
    data = {
        "status": "online"
    }

    return Response(data)


@cache_page(CACHE_TTL)
@api_view(['GET'])
def app_information(request: Request) -> Response:
    """
    Serves the app information found in the AppInformation database table as JSON

    :param request: the request on this route
    :return: the response JSON containing information about the app
    """
    queryset = AppInformation.objects.all()
    serializer = AppInformationSerializer(queryset, many=True)

    return Response(serializer.data)


# https://stackoverflow.com/q/22616973
class MultiSerializerReadOnlyViewSet(ReadOnlyModelViewSet):
    """
    A custom viewset containing ReadOnlyModelViewset's standard list() and retrieve() actions.
    By inheriting this class, the subclass can specify different serializers based on the called actions.
    This is done by specifying the respective key-value-pairs in the supplied dictionary.

    If not overwritten, the list() action will automatically serve all data in the supplied queryset attribute
    as JSON as specified in the respective serializer class on '/<your-route>/'
    If a pagination class is set as well, pagination will be enabled as well.

    If not overwritten, the retrieve() action will automatically serve the data matching the primary key supplied
    in the route itself ('/<your-route>/<pk>/') as JSON as specified in the respective serializer class.

    https://www.django-rest-framework.org/api-guide/viewsets/#readonlymodelviewset
    """

    serializers = dict()

    def get_serializer_class(self):
        return self.serializers.get(self.action, None)

    # As per https://stackoverflow.com/a/51499293
    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


# It is recommended to put all view classes into a single views.py file
# https://stackoverflow.com/a/59393052
class ArtistViewSet(MultiSerializerReadOnlyViewSet):
    """
    A viewset serving information found in the Artist database table.
    """

    queryset = Artist.objects.all()
    pagination_class = APISpecificResultsSetPagination

    serializers = {
        'list': ArtistSerializer,
        'retrieve': ArtistSerializer
    }

    def retrieve(self, request, *args, **kwargs):
        # Instead of returning a specific element, we reply with an HTTP 418 response
        # as only lists are consumed in the frontend for now
        return Response(dict(), status=status.HTTP_418_IM_A_TEAPOT)

    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class ArtworkViewSet(MultiSerializerReadOnlyViewSet):
    """
    A viewset serving information found in the Artwork database table.
    """

    # We prefetch the data specified in ManyToManyFields to minimize database queries
    # and thus improve performance.
    # https://docs.djangoproject.com/en/4.0/ref/models/querysets/#prefetch-related
    queryset = Artwork.objects.filter(published=True).prefetch_related(
        'images',
        Prefetch(
            'linked_artwork',
            queryset=Artwork.objects.filter(published=True)
        )
    )
    pagination_class = APISpecificResultsSetPagination

    serializers = {
        'list': ArtworkSerializer,
        'retrieve': ArtworkSerializer
    }

    def retrieve(self, request, *args, **kwargs):
        # Instead of returning a specific element, we reply with an HTTP 418 response
        # as only lists are consumed in the frontend for now
        return Response(dict(), status=status.HTTP_418_IM_A_TEAPOT)

    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class ExhibitionViewSet(MultiSerializerReadOnlyViewSet):
    """
    A viewset serving information found in the Artwork database table.
    """

    # We prefetch the data specified in ManyToManyFields to minimize database queries
    # and thus improve performance.
    # https://docs.djangoproject.com/en/4.0/ref/models/querysets/#prefetch-related
    queryset = Exhibition.objects.filter(published=True).prefetch_related(
        Prefetch(
            'artworks',
            queryset=Artwork.objects.filter(published=True)
        ),
        Prefetch(
            'exhibitionartwork_set',
            queryset=ExhibitionArtwork.objects.filter(artwork__published=True)
        )
    )
    pagination_class = APISpecificResultsSetPagination

    serializers = {
        'list': ExhibitionSerializer,
        'retrieve': ExhibitionSerializer
    }

    def retrieve(self, request, *args, **kwargs):
        # Instead of returning a specific element, we reply with an HTTP 418 response
        # as only lists are consumed in the frontend for now
        return Response(dict(), status=status.HTTP_418_IM_A_TEAPOT)

    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class MediaViewSet(ReadOnlyModelViewSet):
    """
    A viewset serving information found in the Artwork database table.
    """

    queryset = Media.objects.all()
    serializer_class = MediaSerializer

    def list(self, request, *args, **kwargs):
        # Instead of returning all media objects, we reply with an HTTP 418 response
        # as media objects shall not be listed.
        return Response(dict(), status=status.HTTP_418_IM_A_TEAPOT)

    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)


class CopyrightViewSet(ReadOnlyModelViewSet):
    """
    A viewset serving all relevant Copyright URLs for all displayed Media
    """
    queryset = Media.objects.exclude(
        copyright__isnull=True).exclude(
            copyright="").filter(
                Q(artworks__published=True) | Q(exhibition__published=True)).order_by(
                    "copyright", "copyright_url").values(
                        "copyright", "copyright_url").distinct()

    serializer_class = MediaCopyrightSerializer
    pagination_class = APISpecificResultsSetPagination

    @method_decorator(cache_page(CACHE_TTL))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)
