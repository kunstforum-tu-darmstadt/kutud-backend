// Eventlistener for the Language
$(document).ready(function() {
  var currentLanguage = CURRENT_LANGUAGE;
  var selectLangElement = $('#select_lang');
  hideLanguageFields(currentLanguage, selectLangElement);
  
  selectLangElement.change(function() {
    var selectedValue = $(this).val();
    updateAdminLanguage(selectedValue);
    hideLanguageFields.call(this, selectedValue, selectLangElement);
  });
});

// Function to update the CURRENT_LANGUAGE variable
function updateAdminLanguage(languageCode) {
  // Send an AJAX request to update the admin language
  $.ajax({
    url: '/set-admin-language/',
    method: 'POST',
    headers: {
      'X-CSRFToken': getCookie('csrftoken')
    },
    data: {
      language: languageCode
    },
    success: function(response) {
    },
    error: function(xhr, status, error) {
      console.log(error);
    }
  });
}

// Function to hide all Language Fields besides the selected one
function hideLanguageFields(selectedValue, selectLangElement) {
  $('option', selectLangElement).each(function() {
    var languageCode = $(this).val();
    if (languageCode == selectedValue) {
      $('div.form-row[class$="_' + languageCode + '"]').show();
    } else {
      $('div.form-row[class$="_' + languageCode + '"]').hide();
    }
  });
}

// Function to retrieve the CSRF token from the cookie
function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i].trim();
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}
