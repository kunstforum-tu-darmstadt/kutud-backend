from django.urls import path
from rest_framework.routers import SimpleRouter

import management.views

# The router automatically creates urlpatterns based on the actions defined in the viewset.
# https://www.django-rest-framework.org/api-guide/routers/#simplerouter
router = SimpleRouter()
router.register(r'exhibitions', management.views.ExhibitionViewSet, basename='exhibition')
router.register(r'artworks', management.views.ArtworkViewSet, basename='artwork')
router.register(r'artists', management.views.ArtistViewSet, basename='artist')
router.register(r'media', management.views.MediaViewSet, basename='media')
router.register(r'copyright', management.views.CopyrightViewSet, basename='copyright')

urlpatterns = [
    path('', management.views.api_status),
    path('app_information/', management.views.app_information)
]

urlpatterns += router.urls
