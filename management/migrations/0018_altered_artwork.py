# Generated by Django 4.2.2 on 2023-08-21 11:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0017_published_status_exception'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artwork',
            name='campus',
            field=models.ForeignKey(blank=True, help_text='The campus where the artwork is located (required)', null=True, on_delete=django.db.models.deletion.PROTECT, to='management.campus'),
        ),
        migrations.AlterField(
            model_name='artwork',
            name='linked_artwork',
            field=models.ForeignKey(blank=True, help_text='A referenced artwork from another exhibition (optional)<br>Click the lookup button to search existing artworks.<br>The ID will be set automatically after choosing an artwork.', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='management.artwork'),
        ),
    ]
