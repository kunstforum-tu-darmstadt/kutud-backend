# Generated by Django 3.2.19 on 2023-06-14 19:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('management', '0013_linked_artwork_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='media',
            name='copyright_url',
            field=models.CharField(blank=True, help_text='Website URL of the person owning the copyright of this media file.', max_length=200, null=True),
        ),
    ]
