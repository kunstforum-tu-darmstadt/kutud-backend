def current_language(request):
    language = request.session.get('language') or request.COOKIES.get('language')
    return {'CURRENT_LANGUAGE': language}
