import humanize
from django import forms
from django.contrib import admin
from django.http import HttpRequest
from django.utils.html import format_html
from django.utils.safestring import SafeString
from import_export.resources import ModelResource
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.models import Media


def image_tag(url: str, height: int) -> SafeString:
    """
    Returns an html <img> tag for the given image with the specified height.

    Source: https://stackoverflow.com/a/40715745/4106848

    :param url: the url of the image
    :param height: the display height of the image (unit: pixels)
    :return: an <img> html tag showing the provided image with the given height
    """
    return format_html('<img src="{}" height="{}" />'.format(url, height))


def audio_tag(url: str, media_type: str) -> SafeString:
    """
    Returns an html <audio> tag for playing the given audio file.

    See: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio

    :param url: the url of the audio file
    :param media_type: the MIME type of the audio file
    :return: an <audio> html tag for the provided audio file
    """
    return format_html('<audio controls><source src="{}" type="{}"></audio>'.format(url, media_type))


class MediaAdminForm(forms.models.ModelForm):
    """
    A form to provide hints for the accepted file types.
    """
    def clean(self):
        cleaned_data = super().clean()

        if any(self.errors):
            return cleaned_data

        copyright_url = cleaned_data.get('copyright_url')
        if copyright_url is not None and copyright_url != '':
            try:
                URLValidator(schemes=['http', 'https'])(copyright_url)
            except ValidationError:
                self.add_error('copyright_url', 'Please provide a valid URL.')

        return cleaned_data

    class Meta:
        # A hint for the web browser to choose the correct file type
        # https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/accept
        widgets = {
            'file': forms.FileInput(attrs={'accept': '.jpg,.jpeg,image/png,audio/mpeg'})
        }


class MediaAdmin(CreatorModelAdmin):

    def save_related(self, request, form, formsets, change):
        # Save the related objects first
        super().save_related(request, form, formsets, change)

        # If a new media entry was added and has a copyright_url, update related entries
        if not change and form.cleaned_data.get('copyright_url'):
            added_media = form.instance
            Media.objects.filter(
                copyright=added_media.copyright).exclude(
                    pk=added_media.pk).update(copyright_url=added_media.copyright_url)

    def save_model(self, request, obj, form, change):
        # Save the media object first
        super().save_model(request, obj, form, change)

        # If the copyright_url has been changed, update other entries with the same copyright
        if change and form.cleaned_data.get('copyright_url'):
            Media.objects.filter(
                copyright=obj.copyright).exclude(
                    pk=obj.pk).update(copyright_url=obj.copyright_url)

    @admin.display(description='Media')
    def media_preview_tag(self, obj: Media) -> SafeString | None:
        """
        Returns an html tag showing a preview of the media file.
        Images are displayed with a height of 50px.
        None is returned if the type the media type is not supported.

        :param obj: an instance of the database model Media
        :return: an html tag for previewing the media file
        """
        if obj.type == 'audio/mpeg':
            return audio_tag(obj.file.url, obj.type)
        elif obj.type.startswith('image/'):
            return image_tag(obj.file.url, 50)

    @admin.display(description='Media')
    def media_show_tag(self, obj: Media) -> SafeString | None:
        """
        Returns an html tag for accessing the media file.
        Images are displayed with a height of 200px.
        None is returned if the type the media type is not supported.

        :param obj: an instance of the database model Media
        :return: an html tag for showing the media file
        """
        if obj.type == 'audio/mpeg':
            return audio_tag(obj.file.url, obj.type)
        elif obj.type.startswith('image/'):
            return image_tag(obj.file.url, 200)

    @admin.display(description='Size', empty_value='-')
    def humanized_file_size(self, obj: Media) -> str | None:
        """
        Returns the file size of the stored media file in a human friendly format.
        If the file can't be found, None is returned.

        :param obj: an instance of the database model Media
        :return: the file size of media file in a human friendly format
        """
        try:
            if obj.file and obj.file.size:
                return humanize.naturalsize(obj.file.size, binary=True, format="%.3f")
        except FileNotFoundError:
            pass

        return "?"

    def get_readonly_fields(self, request: HttpRequest, obj: Media = None):
        # Only allow editing the field 'type' when newly creating media objects
        fields = readonly_fields_creator_model + ('uuid', 'type', 'media_show_tag', 'humanized_file_size')
        if obj is not None:
            fields += ('category',)
        return fields

    list_display = ['description', 'media_preview_tag', 'category', 'humanized_file_size', 'editor', 'updated_at']
    search_fields = ['description', 'uuid']
    list_filter = ['category', 'updated_at']
    fieldsets = (
        (None, {
            'fields': ('description', 'copyright', 'copyright_url', 'category', 'media_show_tag')
        }),
        ('File', {
            'fields': ('file', 'type', 'humanized_file_size')
        }),
        internal_attributes_field_set('uuid')
    )
    form = MediaAdminForm


class MediaResource(ModelResource):

    class Meta:
        model = Media
        fields = ('uuid', 'description', 'copyright', 'copyright_url',
                  'category', 'type') + exported_resource_fields_creator_model
