import urllib.parse
import zipfile
from decimal import Decimal
from django.http.request import HttpRequest

import qrcode
import qrcode.image.svg
from django import forms
from django.contrib import admin, messages
from django.db.models import QuerySet
from django.forms import NumberInput
from django.http import HttpResponse
from django.utils.html import format_html
from django.utils.safestring import SafeString
from django.utils.text import slugify
from django.utils.translation import ngettext
from import_export.resources import ModelResource
from django.conf import settings

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.admin.media import image_tag
from management.models import AppInformation, Artwork, ArtworkMedia, ExhibitionArtwork, Campus
from modeltranslation.admin import TranslationAdmin
from modeltranslation.utils import build_localized_fieldname
from management.models.app_information_choices import CHOICE_APP_WEBSITE


def google_maps_link(latitude: Decimal, longitude: Decimal) -> SafeString:
    """
    Returns an <a> html tag linking to the specified position in Google Maps.
    A pin is shown at the given coordinates (latitude & longitude).

    See: https://stackoverflow.com/a/42330709/4106848

    :param latitude: the latitude of the target position as a decimal number in decimal degrees
    :param longitude: the latitude of the target position as a decimal number in decimal degrees
    :return: an <a> html tag linking to the position on Google Maps
    """
    return format_html(
        '<a href="https://maps.google.com/maps?q={},{}" target="_blank">View on Google Maps</a>'
        .format(f'{latitude:.7f}', f'{longitude:.7f}')
    )


def truncate_decimal_places(decimal: Decimal) -> Decimal:
    """
    Truncates a number stored as a Decimal object to seven decimal places

    :param decimal: the decimal that is to be truncated to 7 decimal places
    :return: a Decimal with 7 decimal places
    """

    return decimal.quantize(Decimal(10) ** -7)


class CoordinateInput(NumberInput):
    """
    Shows the Decimal number always with exactly seven decimal places.

    See: https://stackoverflow.com/a/1352985/4106848
    """

    def render(self, name, value, attrs=None, renderer=None):
        if isinstance(value, Decimal):
            value = f'{value:.7f}'
        return super().render(name, value, attrs, renderer)


class CoordinateFormField(forms.DecimalField):
    """
    Truncates the entered number to 7 decimal places.

    See: https://stackoverflow.com/a/37963167/4106848
    """

    # Use the widget to prevent Decimal numbers from using scientific notation
    widget = CoordinateInput

    def to_python(self, value):
        value = super().to_python(value)
        if value is not None:
            return truncate_decimal_places(value)
        return value


class ArtworkMediaInlineFormset(forms.models.BaseInlineFormSet):
    """
    A form set with validation for the artwork media inline admin model.
    It's validated that at least one image is added to an artwork when publishing it.
    """

    def clean(self):
        cleaned_data = super().clean()
        if any(self.errors):
            return cleaned_data

        artwork = self.instance

        # https://stackoverflow.com/a/1884760/4106848
        count = 0
        for form in self.forms:
            if form.cleaned_data and not form.cleaned_data.get('DELETE', False):
                count += 1
        if count < 1 and artwork.published:
            raise forms.ValidationError(
                'Please add at least one image to the artwork in order to publish it'
            )


class ArtworkAdminForm(forms.models.ModelForm):
    """
    A form to truncate decimal places of the latitude and longitude fields.
    """
    def clean_fields_for_linked_artwork_description(self, cleaned_data: dict) -> None:
        """
        If 'linked_artwork is not set but the 'linked_artwork_description' is -> raise an error
        This is localized and prioritizes the first language in LANGUAGES (settings.py)

        :param cleaned_data: the cleaned data
        """
        language_code = settings.LANGUAGES[0][0]
        field_name = build_localized_fieldname('linked_artwork_description', language_code)
        field_value = cleaned_data.get(field_name)

        if field_value is not None or field_value == '':
            self.add_error(field_name, 'The description requires a linked artwork to be set.')

    def clean_fields_for_publishing(self, cleaned_data: dict) -> None:
        """
        Checks that the fields 'identifier', 'info_text', and 'cover_image' are set.
        If not, an error is added to each field.

        :param cleaned_data: the cleaned data
        """
        field_names = ['info_text', 'cover_image']
        language_code = settings.LANGUAGES[0][0]
        for field_name in field_names:
            if field_name == 'info_text':
                field_name = build_localized_fieldname(field_name, language_code)
            field_value = cleaned_data.get(field_name)
            if field_value is None or field_value == '':
                self.add_error(field_name, 'This field is required for publishing an artwork')

    def clean(self):
        cleaned_data = super().clean()
        # Checks if the linked_artwork is set
        if cleaned_data.get('linked_artwork') is None:
            self.clean_fields_for_linked_artwork_description(cleaned_data)

        campus = cleaned_data.get('campus')
        if campus is None:
            self.add_error('campus', "Please set a campus first")
            return cleaned_data

        if self.instance.pk is not None:
            artwork = Artwork.objects.get(pk=self.instance.pk)

            rebuild_id = False

            language_code = settings.LANGUAGES[0][0]
            field_name = build_localized_fieldname('name', language_code)
            name = cleaned_data.get(field_name)

            campus = cleaned_data['campus']
            if campus != artwork.campus:
                rebuild_id = True
            if artwork.identifier is not None:
                if len(artwork.identifier) != 5:
                    rebuild_id = True

            # check if there is a need to build the artworks ID
            if name is not None or name != '':
                if not artwork.identifier or rebuild_id:
                    lookup = {field_name: campus}
                    campusObj = Campus.objects.get(**lookup)
                    self.instance.identifier = artwork.generate_id(name, campusObj.prefix)

        # Check for all required fields when publishing
        new_publish = cleaned_data.get('published')
        if new_publish:
            self.clean_fields_for_publishing(cleaned_data)

        return cleaned_data

    class Meta:
        # Overwrites the default field class for latitude & longitude to allow for automatic truncation of
        # given numbers. The values are always shown with seven decimal places.
        # See: https://docs.djangoproject.com/en/4.0/topics/forms/modelforms/#overriding-the-default-fields
        field_classes = {
            'latitude': CoordinateFormField,
            'longitude': CoordinateFormField,
        }


class ArtworkExhibitionInline(admin.StackedInline):
    """
    A stacked inline administrative model for the database model ExhibitionArtwork.
    It's sorted by the index field.
    Allows Exhibitions to be added from the Artwork.
    """
    @admin.display(description='Cover image')
    def cover_image_tag(self, obj: ExhibitionArtwork) -> SafeString | None:
        """
        If the artwork has a cover_image, an html <img> tag with the fixed height of 75px is returned.
        Otherwise, None.

        :param obj: an instance of the database model ExhibitionArtwork
        :return: if the artwork has a cover_image, an html <img> tag
        """
        if obj.exhibition.cover_image is not None:
            return image_tag(obj.exhibition.cover_image.file.url, 75)

    @admin.display(boolean=True, description="Published")
    def exhibition_published(self, obj: ExhibitionArtwork) -> bool:
        """
        Returns a boolean whether the linked artwork is published.

        :param obj: an instance of the database model ExhibitionArtwork
        :return: whether the linked artwork is published
        """
        return obj.exhibition.published

    model = ExhibitionArtwork
    readonly_fields = ('cover_image_tag', 'exhibition_published')
    fields = ['exhibition', 'cover_image_tag', 'exhibition_published']
    max_num = 1


class ArtworkMediaInline(admin.StackedInline):
    """
    A stacked inline administrative model for the database model ArtworkMedia.
    It's sorted by the index field.
    Due to the database model, only images can be added to the artwork.
    At maximum 15 images can be added to a single artwork.
    """

    @admin.display(description='Image')
    def artwork_image_tag(self, obj: ArtworkMedia) -> SafeString:
        """
        Returns an <img> html tag showing a preview of the image.
        The image is scaled to a height of 75px.

        :param obj: an instance of the ArtworkMedia database model
        :return: an <img> html tag
        """
        return image_tag(obj.media.file.url, 75)

    model = ArtworkMedia
    readonly_fields = ('artwork_image_tag',)
    fields = ['media', 'artwork_image_tag', 'index']
    ordering = ('index',)
    extra = 2
    max_num = 15
    formset = ArtworkMediaInlineFormset


class ArtworkAdmin(TranslationAdmin, CreatorModelAdmin):
    """
    An administrative model for the database model Artwork with additional functionality to
    open the coordinates on Google Maps and to edit the images of the artwork.
    """

    def save_model(self, request, obj, form, change):
        if not change:
            language_code = settings.LANGUAGES[0][0]
            field_name = build_localized_fieldname('name', language_code)
            name = getattr(obj, field_name)
            campus = getattr(obj, "campus").prefix
            obj.identifier = obj.generate_id(name, campus)
            obj.save()
        super().save_model(request, obj, form, change)

    @admin.display(description='Coordinates')
    def google_maps_link(self, obj: Artwork) -> SafeString | None:
        """
        If latitude and longitude of the artwork are both set,
        an <a> element linking to their position on Google Maps is returned.
        If not, None is returned.

        :param obj: an instance of the Artwork database model
        :return: <a> element linking to Google Maps if latitude & longitude are set
        """
        if obj.latitude is not None and obj.longitude is not None:
            return google_maps_link(obj.latitude, obj.longitude)

    # noinspection PyProtectedMember
    @admin.action(permissions=['view'], description='Generate & download QR codes')
    def generate_qr_codes_action(self, request: HttpRequest, queryset: QuerySet) -> HttpResponse | None:
        """
        An action to download QR codes for the selected artworks.

        See: https://stackoverflow.com/a/49220016/4106848

        :param request: the http request for the action
        :param queryset: the queryset of selected objects
        :return: a http response with a file download or None if a message should be shown
        """

        # Limit the number of QR codes in a single to ZIP file to 50 to prevent request timeouts
        if queryset.count() > 50:
            self.message_user(request, 'You can only download up to 50 QR codes at once.', messages.WARNING)
            return

        # Abort if an artwork has an empty identifier
        empty_identifier = queryset.filter(identifier=None).count()
        if empty_identifier > 0:
            verbose_name = self.model._meta.verbose_name
            verbose_name_plural = self.model._meta.verbose_name_plural
            self.message_user(request, ngettext(
                f'The identifier of {empty_identifier} selected {verbose_name} is empty. '
                f'Unselect it and start the download again.',
                f'The identifier of {empty_identifier} selected {verbose_name_plural} is empty. '
                f'Unselect them and start the download again.',
                empty_identifier
            ), messages.WARNING)
            return

        # Get the current website url for the app, if not set it is ignored
        try:
            app_info = AppInformation.objects.get(key=CHOICE_APP_WEBSITE)
            website = app_info.text
        except AppInformation.DoesNotExist:
            self.message_user(
                request,
                'A website URL is required to generate QR codes. '
                'Create a new app information object with the key \'Website for the app\' and set a corresponding URL.',
                messages.WARNING
            )
            return

        # Initiate the response and prepare the ZIP file
        response = HttpResponse(content_type='application/zip')
        zip_file = zipfile.ZipFile(response, 'w')

        # Loop through all selected artworks and adds QR codes to the ZIP file
        qr_code_factory = qrcode.image.svg.SvgPathImage
        for artwork in queryset:
            # Apply percent-encoding to the artwork identifier to allow for correct parsing of special characters
            url = f'{website}{urllib.parse.quote_plus(artwork.identifier)}'
            # Convert the numeric and string identifier of an artwork to a valid filename using Django's slug function
            # See: https://stackoverflow.com/a/295466/4106848
            filename = f'qr-code-{artwork.id}-{slugify(artwork.identifier)}.svg'

            # Create the QR code and store it as an SVG file
            img = qrcode.make(url, image_factory=qr_code_factory)
            svg_bytes = img.to_string()
            zip_file.writestr(filename, svg_bytes)

        # Provide the ZIP file as a download with the name qr-codes.zip
        response['Content-Disposition'] = 'attachment; filename=qr-codes.zip'

        # Return the response
        return response

    def get_readonly_fields(self, request: HttpRequest, obj: Artwork = None):
        # Require the permission 'publish_artwork' to publish artworks
        fields = ('google_maps_link', 'id') + readonly_fields_creator_model
        if not request.user.has_perm('management.publish_artwork'):
            fields += ('published',)
        fields += ("identifier", )
        return fields

    language_code = settings.LANGUAGES[0][0]
    field_name = build_localized_fieldname('name', language_code)
    list_display = [field_name, 'artist', 'identifier', 'published', 'editor', 'updated_at']
    search_fields = [field_name, 'identifier']
    raw_id_fields = ['linked_artwork']
    list_filter = ['artist']
    fieldsets = (
        (None, {
            'fields': ('name', 'identifier', 'published')
        }),
        ('Data', {
            'fields': ('year', 'type', 'canvas', 'artist', 'owner', 'acquired', 'info_text',
                       'linked_artwork', 'linked_artwork_description')
        }),
        ('Media', {
            'fields': ('cover_image', 'audio_guide', 'video_link')
        }),
        ('Location', {
            'fields': ('latitude', 'longitude', 'google_maps_link', 'location', 'address', 'campus')
        }),
        internal_attributes_field_set('id')
    )
    inlines = [ArtworkExhibitionInline, ArtworkMediaInline]
    actions = ['generate_qr_codes_action']
    form = ArtworkAdminForm


class ArtworkResource(ModelResource):
    class Meta:
        model = Artwork
        fields = (
            'id', 'identifier', 'name', 'published', 'year', 'type', 'canvas', 'artist__name', 'owner__name',
            'acquired', 'info_text', 'linked_artwork__identifier', 'linked_artwork_description', 'cover_image',
            'audio_guide', 'video_link', 'images', 'latitude', 'longitude', 'location', 'address',
            'campus__name'
        ) + exported_resource_fields_creator_model
