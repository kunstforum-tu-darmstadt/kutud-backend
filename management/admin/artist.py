from import_export.resources import ModelResource

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.models import Artist
from modeltranslation.admin import TranslationAdmin


class ArtistAdmin(TranslationAdmin, CreatorModelAdmin):
    """
    An administrative model for the database model Artist.
    """

    readonly_fields = ('id',) + readonly_fields_creator_model
    list_display = ['name', 'year_birth', 'year_death', 'editor', 'updated_at']
    search_fields = ['name']
    fieldsets = (
        (None, {
            'fields': ('name', 'year_birth', 'year_death', 'info_text')
        }),
        internal_attributes_field_set('id')
    )


class ArtistResource(ModelResource):

    class Meta:
        model = Artist
        fields = ('id', 'name', 'year_birth', 'year_death', 'info_text') + exported_resource_fields_creator_model
