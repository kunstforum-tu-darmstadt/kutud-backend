from django.contrib import admin
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse
from datetime import datetime


class ManagementAdminSite(admin.AdminSite):
    """
    A custom admin site instance to overwrite the default configuration.

    https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#customizing-the-adminsite-class
    """

    site_header = "Kunstforum administration"
    site_title = "Kunstforum app content admin"

    def get_urls(self):
        """
        Modifies the URLconf of the admin interface.
        Adds a new route 'export/' for exporting all data in a human-readable format.

        https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#adding-views-to-admin-sites
        """
        urls = super().get_urls()
        my_urls = [
            path('export/', self.admin_view(self.export_view), name='export')
        ]
        return my_urls + urls

    def export_view(self, request: HttpRequest) -> HttpResponse:
        """
        A custom view to display the export site and to start the export process.

        :param request: the http request from the user
        :return: an http response with either the page, the zip file or a redirect to the index page
        """
        # Check if the user has the permissions to access the export function
        if not request.user.has_perm('management.export_data'):
            # If not, redirect the user to the index page
            # https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#reversing-admin-urls
            return HttpResponseRedirect(reverse('admin:index'))

        if request.method == 'POST':
            # Import only on call to get as little overhead as possible
            import management.export as export

            # Initiate the response and prepare the ZIP file
            # https://stackoverflow.com/a/49220016/4106848
            response = HttpResponse(content_type='application/zip')
            export.get_zipped_data(response)

            # Provide the ZIP file as a download with the name export.zip
            now = datetime.now()
            filename = f'kunstforum_export_{now.strftime("%d-%m-%Y_%H-%M-%S")}.zip'
            response['Content-Disposition'] = f'attachment; filename={filename}'

            # Return the response
            return response
        else:
            # Return a site to initiate the download
            context = dict(
                self.each_context(request),
                title='Export data',
            )
            return TemplateResponse(request, 'management/export.html', context)
