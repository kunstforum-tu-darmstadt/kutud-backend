from gettext import ngettext

from django.contrib import admin, messages
from django.core.exceptions import ValidationError
from django.db.models import IntegerField, QuerySet
from django.forms import ModelForm
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.urls import path, reverse

from management.forms import ChangeEditorForm
from management.models import CreatorTimeInfo

readonly_fields_creator_model = ('updated_at', 'updated_by', 'created_at', 'created_by')

exported_resource_fields_creator_model = (
    'editor__username', 'created_by__username', 'updated_by__username', 'created_at', 'updated_at'
)


def internal_attributes_field_set(id_name: str | None) -> tuple:
    """
    Creates a field set for internal attributes which is hidden by default.
    The field set starts with the field id_name if the parameter is not None.

    :param id_name: the name of the identifier of the database model
    :return: a tuple for ModelAdmin.fieldsets property
    """
    fields = ('editor',) + readonly_fields_creator_model
    if id_name is not None:
        fields = (id_name,) + fields

    return ('Internal attributes', {
        'classes': ('collapse',),
        'fields': fields
    })


def save_user(request: HttpRequest, obj: CreatorTimeInfo, change: bool) -> None:
    """
    If the requesting user is set, he/she is saved either as the creator or as the updater.
    If the object has been newly created, he/she is also saved as its editor.

    :param request: the http request with a user field set
    :param obj: an instance of a database model extending the abstract model CreatorTimeInfo
    :param change: whether the data has been changed or newly created
    """
    if hasattr(request, 'user') and request.user is not None:
        obj.updated_by = request.user
        if not change:
            obj.created_by = request.user
            obj.editor = request.user


# We're accessing the _meta attribute of the model to determine its name.
# This approach is also used by the class admin.ModelAdmin.
# PyCharm doesn't like this, therefore we disable the check.
# noinspection PyProtectedMember
class CreatorModelAdmin(admin.ModelAdmin):
    """
    An abstract administrative model for the abstract database model CreatorTimeInfo.

    All default permissions provided by Django are not changed in function.
    A new permission 'change_editor' is added.
    This permission only allows users to edit the model if he/she is the assigned editor of the object.
    It utilized for the user group named 'Editor'.

    Furthermore, the properties of the database model are updated when data is persisted.
    Take a look at the function save_user for more details.
    """

    def urlconf_name_model(self):
        """
        Gets the URLconf prefix for the model of this page.

        :return: URLconf prefix for the model
        """
        # Follows the schema of https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#reversing-admin-urls
        app_label = self.model._meta.app_label
        model_name = self.model._meta.model_name
        return f'{app_label}_{model_name}'

    def urlconf_name_change_editor(self):
        """
        Gets the URLconf name for the change editor page.

        :return: URLconf name for the change editor page
        """
        return f'{self.urlconf_name_model()}_change-editor'

    @admin.action(permissions=['change_all'], description='Change editor')
    def change_editor_action(self, request: HttpRequest, queryset: QuerySet) -> HttpResponse:
        """
        An action to redirect to a page for changing the editor of multiple objects.

        :param request: the http request for the action
        :param queryset: the queryset of selected objects
        :return: a http redirect response to the page for choosing the new editor
        """
        # Gets the URL for page to change editors
        url = reverse(f'admin:{self.urlconf_name_change_editor()}')

        # Joins the primary keys of all selected objects to a string
        # The field 'pk' always belongs to the primary key of the model
        # Source: https://stackoverflow.com/a/2165880/4106848
        selected = queryset.values_list('pk', flat=True)
        selected_string = ','.join(str(pk) for pk in selected)

        # Redirects the custom change editor page
        return HttpResponseRedirect(f'{url}?ids={selected_string}')

    def change_editor_view(self, request: HttpRequest) -> HttpResponse:
        """
        A view to change the editor of multiple objects at once.
        The selected objects are encoded in the GET parameter 'ids'.
        It contains the string representations of the primary key fields joined with commas.

        The view was constructed using the function django.contrib.auth.admin.user_change_password as a template.

        :param request: the request for this view
        :return: an http response either showing the form to select the new editor or redirecting to the list page
        """
        # Permission 'change_{model}' is required to change the editor
        model_name = self.model._meta.model_name
        if not request.user.has_perm(f'management.change_{model_name}'):
            # If user doesn't have the permission, return to the list page and show an error
            self.message_user(
                request,
                'No permission to change editors of selected objects',
                messages.WARNING
            )
            return HttpResponseRedirect(reverse(f'admin:{self.urlconf_name_model()}_changelist'))

        # Get the raw string of selected objects from the GET parameters
        ids = request.GET.get(key='ids', default='')

        # Convert the selected identifiers to integers if the primary key field stores integers
        try:
            selected = ids.split(',')
            if isinstance(self.model._meta.pk, IntegerField):
                selected = list(map(int, selected))
            queryset = QuerySet(model=self.model).filter(pk__in=selected)
        except (ValidationError, ValueError):
            # If the given parameters are invalid in any way, abort and redirect to the list page with an error shown
            self.message_user(
                request,
                'Invalid parameters found, therefore editors cannot be changed',
                messages.WARNING
            )
            return HttpResponseRedirect(reverse(f'admin:{self.urlconf_name_model()}_changelist'))

        # If there are no objects matching the given identifiers, abort and redirect to the list page with an error
        if queryset.count() == 0:
            self.message_user(
                request,
                'No objects with the given identifiers found, therefore editors can not be changed',
                messages.WARNING
            )
            return HttpResponseRedirect(reverse(f'admin:{self.urlconf_name_model()}_changelist'))

        verbose_name = self.model._meta.verbose_name
        verbose_name_plural = self.model._meta.verbose_name_plural

        if request.method == 'POST':
            # A form was submitted, so we're reading the parameters and if correct applying the changes to editors
            form = ChangeEditorForm(queryset, request.POST)
            if form.is_valid():
                # Apply the requested changes e.g. change the editor of the selected objects
                updated, editor = form.save()
                # Show a success message
                editor_name = editor.username
                self.message_user(request, ngettext(
                    f'The editor of {updated} {verbose_name} was changed to {editor_name}',
                    f'The editor of {updated} {verbose_name_plural} was changed to {editor_name}',
                    updated
                ), messages.SUCCESS)
                # Redirect to the list of all objects
                return HttpResponseRedirect(
                    reverse(f'admin:{self.urlconf_name_model()}_changelist'),
                )
        else:
            # The page was just opened using a GET request, so we show the form
            form = ChangeEditorForm(queryset)

        # Context for the template
        context = dict(
            self.admin_site.each_context(request),
            title=f'Change editor of {verbose_name_plural}',
            form=form,
            objects=queryset,
            opts=self.model._meta
        )

        # Required by https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#adding-views-to-admin-sites
        request.current_app = self.admin_site.name
        return TemplateResponse(request, 'management/change_editor.html', context)

    def has_change_all_permission(self, request: HttpRequest, obj: CreatorTimeInfo = None) -> bool:
        """
        A method to check whether the user of the request has the permission to change all objects of this model.
        To determine this, the has_change_permission method of the super class is called.
        The result of this call is returned.

        This permission check is utilized for the change_editor_action.
        See: https://docs.djangoproject.com/en/4.0/ref/contrib/admin/actions/#setting-permissions-for-actions

        :param request: the current http request
        :param obj: the object to check permissions for (if None, permissions are checked for the model as whole)
        :return: whether the request.user has permission to change the object of this model
        """
        return super().has_change_permission(request, obj)

    def get_urls(self):
        # Overwrites the default routes of the model
        # See: https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.get_urls
        urls = super().get_urls()

        my_urls = [
            # Add a route with the locally defined name for the change editor view
            path(
                'change_editor/',
                self.admin_site.admin_view(self.change_editor_view),
                name=self.urlconf_name_change_editor()
            )
        ]

        # Order is important here
        return my_urls + urls

    def has_change_permission(self, request: HttpRequest, obj: CreatorTimeInfo = None) -> bool:
        # If the user has access by other permissions, grant it
        super_permission = super().has_change_permission(request, obj)
        if super_permission:
            return super_permission

        # If not, check if the user has the permission to edit and is the editor
        model_name = self.model._meta.model_name
        if request.user.has_perm(f'management.change_editor_{model_name}'):
            if obj is not None:
                return obj.editor == request.user
            else:
                # Allow editing of objects of the model in general
                # https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#django.contrib.admin.ModelAdmin.has_change_permission
                return True

        return False

    def save_model(self, request: HttpRequest, obj: CreatorTimeInfo, form: ModelForm, change: bool) -> None:
        save_user(request, obj, change)
        super().save_model(request, obj, form, change)

    actions = ['change_editor_action']
