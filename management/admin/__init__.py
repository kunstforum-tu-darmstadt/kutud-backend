from django.contrib.auth.admin import GroupAdmin, UserAdmin
from django.contrib.auth.models import Group, User

from management.admin.app_information import AppInformationAdmin, AppInformationResource
from management.admin.artist import ArtistAdmin, ArtistResource
from management.admin.artwork import ArtworkAdmin, ArtworkResource
from management.admin.campus import CampusAdmin, CampusResource
from management.admin.exhibition import ExhibitionAdmin, ExhibitionResource
from management.admin.media import MediaAdmin, MediaResource
from management.admin.owner import OwnerAdmin, OwnerResource
from management.admin.site import ManagementAdminSite
from management.models import AppInformation, Artist, Artwork, Campus, Exhibition, Media, Owner

# Uses a custom admin site to modify the URLs
# https://docs.djangoproject.com/en/4.0/ref/contrib/admin/#customizing-the-adminsite-class
admin_site = ManagementAdminSite()

# It is required to register admin sites from other apps manually
# https://stackoverflow.com/a/40265975/4106848
admin_site.register(Group, GroupAdmin)
admin_site.register(User, UserAdmin)

# Admins sites from our app
admin_site.register(Artist, ArtistAdmin)
admin_site.register(Artwork, ArtworkAdmin)
admin_site.register(Campus, CampusAdmin)
admin_site.register(Exhibition, ExhibitionAdmin)
admin_site.register(Media, MediaAdmin)
admin_site.register(AppInformation, AppInformationAdmin)
admin_site.register(Owner, OwnerAdmin)
