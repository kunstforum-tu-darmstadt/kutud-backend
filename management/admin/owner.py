from import_export.resources import ModelResource

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.models import Owner


class OwnerAdmin(CreatorModelAdmin):
    """
    An administrative model for the database model Owner.
    """

    readonly_fields = ('id',) + readonly_fields_creator_model
    list_display = ['name', 'editor', 'updated_at']
    search_fields = ['name']
    fieldsets = (
        (None, {
            'fields': ('name',)
        }),
        internal_attributes_field_set('id')
    )


class OwnerResource(ModelResource):

    class Meta:
        model = Owner
        fields = ('id', 'name') + exported_resource_fields_creator_model
