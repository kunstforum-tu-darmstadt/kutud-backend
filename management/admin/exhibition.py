from django import forms
from django.contrib import admin
from django.http import HttpRequest
from django.utils.safestring import SafeString
from import_export.fields import Field
from import_export.resources import ModelResource
from import_export.widgets import ManyToManyWidget
from django.conf import settings

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.admin.media import image_tag
from management.models import Exhibition, ExhibitionArtwork, Artwork
from modeltranslation.admin import TranslationAdmin
from modeltranslation.utils import build_localized_fieldname


class ExhibitionArtworkInlineFormset(forms.models.BaseInlineFormSet):
    """
    A form set with validation for the exhibition artworks inline admin model.
    It's validated that at least one published artwork is added to an exhibition when publishing it.
    """

    def clean(self):
        # https://docs.djangoproject.com/en/4.0/topics/forms/formsets/#custom-formset-validation
        cleaned_data = super().clean()

        if any(self.errors):
            return cleaned_data

        return cleaned_data


class ExhibitionArtworkInline(admin.StackedInline):
    """
    A stacked inline administrative model for the database model ExhibitionArtwork.
    It's sorted by the index field.
    For each artwork the publishing status and the cover image is shown.
    """

    @admin.display(description='Cover image')
    def cover_image_tag(self, obj: ExhibitionArtwork) -> SafeString | None:
        """
        If the artwork has a cover_image, an html <img> tag with the fixed height of 75px is returned.
        Otherwise, None.

        :param obj: an instance of the database model ExhibitionArtwork
        :return: if the artwork has a cover_image, an html <img> tag
        """
        if obj.artwork.cover_image is not None:
            return image_tag(obj.artwork.cover_image.file.url, 75)

    @admin.display(boolean=True, description="Published")
    def artwork_published(self, obj: ExhibitionArtwork) -> bool:
        """
        Returns a boolean whether the linked artwork is published.

        :param obj: an instance of the database model ExhibitionArtwork
        :return: whether the linked artwork is published
        """
        return obj.artwork.published

    model = ExhibitionArtwork
    readonly_fields = ('cover_image_tag', 'artwork_published')
    fields = ['artwork', 'cover_image_tag', 'artwork_published', 'index']
    ordering = ('index',)
    extra = 2
    formset = ExhibitionArtworkInlineFormset


class ExhibitionAdmin(TranslationAdmin, CreatorModelAdmin):
    """
    An administrative model for the database model Exhibition with additional functionality to
    edit the artworks shown in the exhibition.
    """

    def save_model(self, request, obj, form, change):
        # Save the exhibition object first
        super().save_model(request, obj, form, change)
        # Sets the published Status accordingly
        ex = Exhibition.objects.get(pk=obj.pk)
        ex.update_published_status()
        ex.save()

    def get_readonly_fields(self, request: HttpRequest, obj: Exhibition = None):
        # Require the permission 'publish_exhibition' to publish exhibitions
        fields = ('id',) + readonly_fields_creator_model
        fields += ('published', )
        return fields

    language_code = settings.LANGUAGES[0][0]
    field_name = build_localized_fieldname('title', language_code)
    list_display = [field_name, 'start_date', 'end_date', 'published', 'editor', 'updated_at']
    search_fields = [field_name]
    fieldsets = (
        (None, {
            'fields': ('title', 'start_date', 'end_date', 'published', 'published_exception')
        }),
        ('Media', {
            'fields': ('cover_image',)
        }),
        internal_attributes_field_set('id')
    )
    inlines = [ExhibitionArtworkInline]


class ExhibitionResource(ModelResource):
    artworks = Field(attribute="artworks", column_name="artworks", widget=ManyToManyWidget(Artwork, field='name'))

    class Meta:
        model = Exhibition
        fields = ('id', 'title', 'start_date', 'end_date', 'published',
                  'published_exception', 'cover_image', 'artworks')
        fields += exported_resource_fields_creator_model
