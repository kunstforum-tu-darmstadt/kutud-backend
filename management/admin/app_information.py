from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator
from import_export.resources import ModelResource

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.models import AppInformation
from management.models.app_information_choices import CHOICE_APP_WEBSITE
from modeltranslation.utils import build_localized_fieldname
from modeltranslation.admin import TranslationAdmin


class AppInformationAdminForm(forms.ModelForm):
    """
    A form to validate the website URL if the key is selected.
    """

    def clean(self):
        cleaned_data = super().clean()

        if any(self.errors):
            return cleaned_data

        key = cleaned_data.get('key')

        # Check that the website URL is valid if the key is currently selected
        if key == CHOICE_APP_WEBSITE:
            is_mandatory_language = True
            for language_code, _ in settings.LANGUAGES:
                text_field_name = build_localized_fieldname('text', language_code)
                text = cleaned_data.get(text_field_name)

                if is_mandatory_language and not text:
                    raise forms.ValidationError(f'Please provide a URL for the website in {language_code}.')
                elif text:
                    try:
                        URLValidator(schemes=['http', 'https'])(text)
                    except ValidationError as err:
                        self.add_error(text_field_name, err)

                    if not (text.endswith('#') or text.endswith('=') or text.endswith('/')):
                        self.add_error(
                            text_field_name,
                            'The website URL must end with either one of the separators \'#\', \'=\' or \'/\'.')
                is_mandatory_language = False
        return cleaned_data


class AppInformationAdmin(TranslationAdmin, CreatorModelAdmin):
    """
    An administrative model for the database model AppInformation.
    """

    readonly_fields = readonly_fields_creator_model
    list_display = ['key', 'editor', 'updated_at']
    search_fields = ['key']
    fieldsets = (
        (None, {
            'fields': ('key', 'text')
        }),
        internal_attributes_field_set(None)
    )
    form = AppInformationAdminForm


class AppInformationResource(ModelResource):

    class Meta:
        model = AppInformation
        fields = ('key', 'text') + exported_resource_fields_creator_model
