from django import forms
from import_export.resources import ModelResource
from django.conf import settings

from management.admin.creator_time_info import CreatorModelAdmin, internal_attributes_field_set, \
    readonly_fields_creator_model, exported_resource_fields_creator_model
from management.models import Campus
from modeltranslation.admin import TranslationAdmin
from modeltranslation.utils import build_localized_fieldname


class CampusAdminForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()

        if any(self.errors):
            return cleaned_data

        prefix_field_name = 'prefix'
        prefix = cleaned_data.get(prefix_field_name)

        if len(prefix) != 1:
            self.add_error(prefix_field_name, 'The campus prefix must be exactly 1 character long.')
        all_campuses = Campus.objects.all()
        if len(all_campuses) > 0:
            if self.instance.pk is not None:
                current_campus = Campus.objects.get(pk=self.instance.pk)
                if all_campuses.filter(prefix__exact=prefix).count() > 0 and current_campus.prefix != prefix:
                    self.add_error(prefix_field_name, 'The campus prefix does already exist and must be unique.')
            else:
                if all_campuses.filter(prefix__exact=prefix).count() > 0:
                    self.add_error(prefix_field_name, 'The campus prefix does already exist and must be unique.')

        return cleaned_data


class CampusAdmin(TranslationAdmin, CreatorModelAdmin):
    """
    An administrative model for the database model Campus.
    """
    language_code = settings.LANGUAGES[0][0]
    field_name = build_localized_fieldname('name', language_code)
    readonly_fields = ('id',) + readonly_fields_creator_model
    list_display = [field_name, 'editor', 'updated_at']
    search_fields = [field_name, 'prefix']
    fieldsets = (
        (None, {
            'fields': ('name', 'prefix',)
        }),
        internal_attributes_field_set('id')
    )
    form = CampusAdminForm


class CampusResource(ModelResource):

    class Meta:
        model = Campus
        fields = ('id', 'name', 'prefix') + exported_resource_fields_creator_model
