from rest_framework.pagination import PageNumberPagination


# Inspired by https://stackoverflow.com/a/34888758
class APISpecificResultsSetPagination(PageNumberPagination):
    """
    A custom pagination constraint class limiting the amount of items per page returned by the API to 50 by default.
    By supplying a 'page_size' GET parameter, the user is able to increase this value up to 100.
    """

    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 100
