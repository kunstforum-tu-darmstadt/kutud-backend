from management.models import Media

from rest_framework import serializers


class MediaCopyrightSerializer(serializers.ModelSerializer):
    """
    A serializer for the data stored in the Media database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    class Meta:
        model = Media
        fields = ['uuid', 'copyright', 'copyright_url']
        read_only_fields = fields
