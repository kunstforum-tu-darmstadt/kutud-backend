from rest_framework import serializers

from management.models import Artwork
from management.serializers.artwork_media_serializer import ArtworkMediaSerializer
from management.serializers.creator_time_info_serializer import CreatorTimeInfoSerializer
from management.serializers.linked_artwork_serializer import LinkedArtworkSerializer
from management.serializers.util import MediaPrimaryKeyRelatedField
from management.serializers.util import sort_filter_indexed_list_by_index, rename_field_serializations
import datetime
import pytz


class ArtworkSerializer(CreatorTimeInfoSerializer):
    """
    A serializer for the data stored in the Artwork database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    # Needed to properly serialize ManyToMany fields initialized with 'through'
    # https://stackoverflow.com/a/41996831
    # https://stackoverflow.com/a/17263583
    images = ArtworkMediaSerializer(source='artworkmedia_set', many=True)

    cover_image = MediaPrimaryKeyRelatedField(read_only=True)
    audio_guide = MediaPrimaryKeyRelatedField(read_only=True, allow_null=True)

    owner = serializers.StringRelatedField()
    campus = serializers.StringRelatedField()

    linked_artwork = LinkedArtworkSerializer(read_only=True, allow_null=True)

    change_status = serializers.SerializerMethodField('get_changed_status')

    class Meta:
        model = Artwork
        exclude = ['editor', 'created_by', 'updated_by', 'published']
        read_only_fields = ['identifier', 'name', 'year', 'type', 'canvas', 'artist', 'owner', 'acquired', 'info_text',
                            'images', 'cover_image', 'audio_guide', 'video_link', 'latitude', 'longitude', 'location',
                            'address', 'created_at', 'updated_at', 'campus', 'linked_artwork',
                            'linked_artwork_description']

    def get_changed_status(self, obj):
        created = obj.created_at
        updated = obj.updated_at
        days_threshold = 31
        utc = pytz.utc
        now = datetime.datetime.now(utc)
        # Displays how old the entries for the Artworks are.
        if (now - created).days < days_threshold:
            return "new"
        if (now - updated).days < days_threshold:
            return "update"
        return "old"

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        representation["images"] = sort_filter_indexed_list_by_index(representation["images"])
        representation["image_count"] = len(representation["images"])

        return rename_field_serializations(
            representation,
            how_acquired="acquired"
        )
