from management.models import AppInformation
from management.serializers.creator_time_info_serializer import CreatorTimeInfoSerializer


class AppInformationSerializer(CreatorTimeInfoSerializer):
    """
    A serializer for the data stored in the AppInformation database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    class Meta:
        model = AppInformation
        exclude = ['editor', 'created_by', 'updated_by']
        read_only_fields = ['key', 'text', 'created_at', 'updated_at']
