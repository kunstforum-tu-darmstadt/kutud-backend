# Documentation on serializers: https://www.django-rest-framework.org/api-guide/serializers/

# Serializers in use:
from .app_information_serializer import AppInformationSerializer
from .artist_serializer import ArtistSerializer
from .artwork_serializer import ArtworkSerializer
from .exhibition_serializer import ExhibitionSerializer
from .media_serializer import MediaSerializer
from .media_copyright_serializer import MediaCopyrightSerializer
