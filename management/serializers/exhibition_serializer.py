from management.models import Exhibition
from management.serializers.creator_time_info_serializer import CreatorTimeInfoSerializer
from management.serializers.exhibition_artwork_serializer import ExhibitionArtworkSerializer
from management.serializers.util import MediaPrimaryKeyRelatedField
from management.serializers.util import convert_time_fields_to_epoch, sort_filter_indexed_list_by_index


class ExhibitionSerializer(CreatorTimeInfoSerializer):
    """
    A serializer for the data stored in the Exhibition database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    # Needed to properly serialize ManyToMany fields initialized with 'through'
    # https://stackoverflow.com/a/41996831
    # https://stackoverflow.com/a/17263583
    artworks = ExhibitionArtworkSerializer(source='exhibitionartwork_set', many=True)

    cover_image = MediaPrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Exhibition
        exclude = ['editor', 'created_by', 'updated_by', 'published']
        read_only_fields = ['title', 'start_date', 'end_date', 'cover_image', 'artworks', 'created_at', 'updated_at']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        updated_time_repr = convert_time_fields_to_epoch(
            representation,
            "start_date",
            "end_date"
        )

        updated_time_repr["artworks"] = sort_filter_indexed_list_by_index(updated_time_repr["artworks"])
        updated_time_repr["artwork_count"] = len(updated_time_repr["artworks"])

        return updated_time_repr
