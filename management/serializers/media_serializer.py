from management.models import Media
from management.serializers.creator_time_info_serializer import CreatorTimeInfoSerializer
from management.serializers.util import rename_field_serializations


class MediaSerializer(CreatorTimeInfoSerializer):
    """
    A serializer for the data stored in the Media database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    class Meta:
        model = Media
        exclude = ['editor', 'created_by', 'updated_by']
        read_only_fields = ['uuid', 'description', 'category', 'file', 'type', 'copyright',
                            'copyright_url', 'created_at', 'updated_at']

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        return rename_field_serializations(
            representation,
            url="file"
        )
