import typing

import ciso8601  # https://github.com/closeio/ciso8601, much faster than datetime
from rest_framework import serializers


class MediaPrimaryKeyRelatedField(serializers.PrimaryKeyRelatedField):
    """
    Field serializer intended to represent its related Media object by its primary
    key's string representation.
    """

    def to_representation(self, value):
        return str(value.pk)


def to_epoch_unix_timestamp(time: str) -> int:
    """
    Converts timestamp strings in a valid Python datetime format to Unix epochs (https://www.epoch101.com/).
    The returned time is in the GMT+0000 timezone format.

    :param time: the timestamp supplied as a string
    :return: an int conveying the time as a precise Unix epoch
    :raise ValueError: if the time string is empty or None
    """
    if time is None or not time:
        raise ValueError

    return int(ciso8601.parse_datetime(time).timestamp())


def rename_field_serializations(serialized_model: dict, **kwargs) -> dict:
    """
    Renames fields existent in serialized model as specified in kwargs.
    The kwargs key specifies the new identifier and thus key in the serialized model,
    the kwargs value the old identifier of the key.

    :param serialized_model: the serialized model in dictionary format
    :return: a new model serialization with the specified renaming applied
    :raise ValueError: if either kwargs or serialized_model are None
    """
    if serialized_model is None or kwargs is None:
        raise ValueError

    new_model_repr = serialized_model.copy()  # Have to call copy due to reference issues otherwise

    if new_model_repr and kwargs:
        for k, v in kwargs.items():
            new_model_repr[k] = new_model_repr.pop(v)

    return new_model_repr


def convert_time_fields_to_epoch(serialized_model: dict, *args) -> dict:
    """
    Converts all fields of serialized_model specified in args to Unix epochs.

    :param serialized_model: the serialized model in dictionary format
    :return: a new model serialization with the specified time fields represented as Unix epochs
    :raise ValueError: if either args or serialized_model are None
    """
    if serialized_model is None or args is None:
        raise ValueError

    new_model_repr = serialized_model.copy()  # Have to call copy due to reference issues otherwise

    if new_model_repr and args:
        for field in args:
            new_model_repr[field] = to_epoch_unix_timestamp(new_model_repr[field])

    return new_model_repr


def sort_filter_indexed_list_by_index(indexed_list: typing.List[tuple]) -> list:
    """
    Sorts a list consisting of (index, data) tuples by integer index and filters out the indices afterwards,
    thus returning solely a list of now sorted data.

    :param indexed_list: the list of (index, data) tuples
    :return: a sorted list of data objects
    :raise ValueError: if the list is empty or None
    """
    if indexed_list is None:
        raise ValueError

    sorted_by_index = sorted(indexed_list, key=lambda index_data_tuple: index_data_tuple[0])  # Sort by index
    return [data for _, data in sorted_by_index]
