from rest_framework import serializers

from management.models import ArtworkMedia


class ArtworkMediaSerializer(serializers.ModelSerializer):
    """
    A serializer for the data stored in the ArtworkMedia database table.
    It is intended for usage in contexts where the respective ManyToManyFields
    (possibly with usage of 'through') require further attention.
    """

    media_uuid = serializers.UUIDField(source='media.uuid', read_only=True)

    class Meta:
        model = ArtworkMedia
        fields = ['index', 'media_uuid']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation["index"], representation["media_uuid"]
