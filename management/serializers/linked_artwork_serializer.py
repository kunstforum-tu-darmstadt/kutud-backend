from rest_framework import serializers

from management.models import Artwork


class LinkedArtworkSerializer(serializers.ModelSerializer):
    """
    Field serializer intended to represent its related Artwork object only if
    its 'published' value is set to True.
    Otherwise, None is returned
    """

    class Meta:
        model = Artwork
        fields = ['id', 'published']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if representation['published']:
            return representation['id']

        return None
