from management.models import Artist
from management.serializers.creator_time_info_serializer import CreatorTimeInfoSerializer


class ArtistSerializer(CreatorTimeInfoSerializer):
    """
    A serializer for the data stored in the Artist database table.
    It is intended for usage in retrieve() actions or similar contexts.
    """

    class Meta:
        model = Artist
        exclude = ['editor', 'created_by', 'updated_by']
        read_only_fields = ['name', 'year_birth', 'year_death', 'info_text', 'created_at', 'updated_at']
