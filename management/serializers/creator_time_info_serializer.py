from rest_framework import serializers

from management.models import CreatorTimeInfo
from management.serializers.util import convert_time_fields_to_epoch


class CreatorTimeInfoSerializer(serializers.ModelSerializer):
    """
    A serializer converting created_at and updated_at fields to unix timestamps
    """

    class Meta:
        model = CreatorTimeInfo
        fields = ["created_by", "updated_by"]
        read_only_fields = fields

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        updated_time_repr = convert_time_fields_to_epoch(
            representation,
            "created_at",
            "updated_at"
        )

        return updated_time_repr
