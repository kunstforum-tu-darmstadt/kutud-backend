from rest_framework import serializers

from management.models import ExhibitionArtwork


class ExhibitionArtworkSerializer(serializers.ModelSerializer):
    """
    A serializer for the data stored in the ExhibitionArtwork database table.
    It is intended for usage in contexts where the respective ManyToManyFields
    (possibly with usage of 'through') require further attention.
    """

    artwork_id = serializers.ReadOnlyField(source='artwork.id')

    class Meta:
        model = ExhibitionArtwork
        fields = ['index', 'artwork_id']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation["index"], representation["artwork_id"]
