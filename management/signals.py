from django.core.cache import cache
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver
from kunstforum import settings

from management.models.media import Media, media_upload_path
from management.models import Artwork, Exhibition, Campus
from modeltranslation.utils import build_localized_fieldname


@receiver(post_delete, sender=Media)
def delete_file_on_disk(sender: Media, instance: Media, using, **kwargs):
    """
    Deletes media files such as images or audio from disk immediately after they were deleted from the database
    https://docs.djangoproject.com/en/4.0/topics/signals/#connecting-to-signals-sent-by-specific-senders
    https://simpleisbetterthancomplex.com/tutorial/2016/07/28/how-to-create-django-signals.html
    """
    if instance.file:
        # The storage provider to support different backends
        storage = instance.file.storage
        # The path where the file is saved
        file_path = media_upload_path(instance, instance.file.name)
        # Not only delete the file from the database, but also from disk
        storage.delete(file_path)


@receiver(post_save)
def clear_cache_on_save(sender, **kwargs):
    """
    Clears the cache after a model instance has been newly created or updated.

    Signal: https://docs.djangoproject.com/en/4.0/ref/signals/#post-save
    """
    cache.clear()


@receiver(post_delete)
def clear_cache_on_delete(sender, **kwargs):
    """
    Clears the cache after a model instance has been deleted.

    Signal: https://docs.djangoproject.com/en/4.0/ref/signals/#post-delete
    """
    cache.clear()


@receiver(post_save, sender=Artwork)
def force_exhibition_update_from_artwork(sender: Artwork, instance: Artwork, using, **kwargs):
    """
    Fetches all artwork dependent exhibitions and updates them in the db
    """
    relevant_exhibitions = Exhibition.objects.filter(artworks__identifier=instance.identifier).all()
    for ex in relevant_exhibitions:
        ex.update_published_status()
        ex.save()


@receiver(post_save, sender=Campus)
def force_exhibition_update_from_campus(sender: Campus, instance: Campus, using, **kwargs):
    """
    Fetches all campus dependent artworks and updates them in the db
    """
    relevant_artworks = Artwork.objects.filter(campus=instance).all()
    for aw in relevant_artworks:
        language_code = settings.LANGUAGES[0][0]
        field_name = build_localized_fieldname('name', language_code)
        name = getattr(aw, field_name)
        if (len(aw.identifier) != 5 or aw.identifier[0] != instance.prefix):
            identifier = aw.generate_id(name, instance.prefix)
            aw.identifier = identifier
            aw.save()
