#!/bin/bash
# Delete old static files
# https://docs.djangoproject.com/en/3.2/ref/contrib/staticfiles/#django-admin-collectstatic
python3 manage.py collectstatic --clear --noinput

# Wait for database to be completely ready to avoid errors during the migration
# https://github.com/painless-software/django-probes
python3 manage.py wait_for_database

# Run the database migrations
# https://docs.djangoproject.com/en/3.2/ref/django-admin/#django-admin-migrate
python3 manage.py migrate

# Check if all requirements are met for deployment
# https://docs.djangoproject.com/en/3.2/ref/django-admin/#cmdoption-check-deploy
python3 manage.py check --deploy

# Run the project using uvicorn
# https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/uvicorn/
python3 -m gunicorn kunstforum.asgi:application -k uvicorn.workers.UvicornWorker -w "$GUNICORN_WORKER_COUNT" -b :8000
