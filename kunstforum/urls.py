"""kunstforum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django.views.generic import TemplateView

from management.admin import admin_site
from management.views import set_admin_language

urlpatterns = [
    path('admin/', admin_site.urls),  # Custom admin page
    path('set-admin-language/', set_admin_language, name='set_admin_language'),
    path('', TemplateView.as_view(
        template_name='management/index.html'
    ), name='index'),  # Landing page served on /
    path('robots.txt', TemplateView.as_view(
        template_name='management/robots.txt',
        content_type='text/plain'
    ), name='robots'),  # https://developers.google.com/search/docs/advanced/robots/intro
    path('api/', include('management.urls')),  # API
    path('__debug__/', include('debug_toolbar.urls')),  # Debug Toolbar (only enabled for internal ips, see settings.py)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# + static(): Serves media files during development
# See: https://docs.djangoproject.com/en/4.0/howto/static-files/#serving-files-uploaded-by-a-user-during-development
