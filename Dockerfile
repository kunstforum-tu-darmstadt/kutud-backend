# Python 3.10 base image
FROM python:3.10-bullseye

# So that no .pyc files are written to disk as the app will only be executed once inside of the container
ENV PYTHONDONTWRITEYTECODE=1
# So that the outputs (e.g. print statements) are directly sent to the set output device without any buffering
ENV PYTHONUNBUFFERED=1

# Deploy the application to the directory /opt/app
WORKDIR /opt/app

# Install pipenv
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install pipenv

# Copy Pipfile and install dependencies
COPY Pipfile Pipfile.lock /opt/app/
RUN python3 -m pipenv requirements > requirements.txt
RUN python3 -m pip install -r requirements.txt

# Copy all source files into the container
COPY . /opt/app/

# Configure the application to run in production mode
ENV DEBUG=off
ENV STATIC_ROOT=/opt/static/
ENV MEDIA_ROOT=/opt/media/

# Start the application using the entrypoint and expose the port 8000
EXPOSE 8000
CMD ["./docker-entrypoint.sh"]