# Kunstforum Backend

The purpose of this application is to supply
the [Android & iOS app](https://git.rwth-aachen.de/kunstforum-tu-darmstadt/KuTUD)
of the [Kunstforum TU Darmstadt](https://www.tu-darmstadt.de/kunstforum/index.de.jsp) with data & images. Exhibitions
and their corresponding images can be added, edited and deleted using a web interface.

The application is built using the [Python](https://www.python.org/) framework [Django](https://www.djangoproject.com/).
It requires a database and web server to serve static files.

## Table of Contents

You can find documentation related to various topics in the [docs](./docs) folder.

1. Production
    1. [Setup & Update](./docs/setup.md)
    2. [Backup & Restore](./docs/backup.md)
    3. [Groups & Users](./docs/users.md)
    4. [QR Codes](./docs/qr_codes.md)
2. Development
    1. [Development Guide](./docs/development.md)
    2. [API Documentation](./docs/api.md)

## Contributors

This project was created as part of a Projekt-Praktikum at the TU Darmstadt in the summer term 2021 by the team of:
* Aria Jamili
* Betty Ballin
* Mohamad Kattaa
* Muhamad Azem
* Anissa Manai

It was further improved as part of the 
[Bachelor-Praktikum](https://www.informatik.tu-darmstadt.de/studium_fb20/im_studium/studiengaenge_liste/bachelor_praktikum.de.jsp) 
at the TU Darmstadt in the winter term 2021/22 by the team of:
* Louis Rethfeld
* Lukas Arnold
* Paul Jonas Kurz
* Philipp Hempel
* Timo Imhof
